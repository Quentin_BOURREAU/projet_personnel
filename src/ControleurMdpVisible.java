import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;

public class ControleurMdpVisible implements EventHandler<ActionEvent>{

    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet vue  de l'appli
    */
    public ControleurMdpVisible(AppliProjet appli){
        this.appli = appli;
    }

    /**
     * Actions à effectuer lors du clic le bouton visible ou non-visible
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        Map<String, Node> lesElementsGraphiques = this.appli.getLesElementsGraphiques();
        Button boutonClique = (Button) lesElementsGraphiques.get("boutonMdp");
        String idBouton = boutonClique.getId();
        if (idBouton.equals("non-visible")){ // on souhaite voir le mot de passe (qu'il soit en visible)
            boutonClique.setId("visible"); // on change l'id du bouton
            boutonClique.setGraphic(lesElementsGraphiques.get("imageMdpVisible")); // on change l'image dans le bouton
            // changer en TextField
        }
        else{ // on souhaite cacher le mot de passe (qu'il soit en non-visible)
            boutonClique.setId("non-visible");
            boutonClique.setGraphic(lesElementsGraphiques.get("imageMdpNonVisible"));
            // changer en PasswordField
        }
    }   
}
