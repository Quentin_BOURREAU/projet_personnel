import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/** UserSQL */
public class UserSQL {
    /** connexion à la base de donnée */
    private Connection connexion = Connexion.laConnexion;

    /** Constructeur de UserSQL */
    public UserSQL(){
    }

    /**
     * renvois l'utilisateur correspondant au nom donné 
     * @param pseudo nom de l'utilisateur
     * @return L'utilisateur
     * @throws UtilisateurInvalideException il n'y a aucun utilisateur avec ce nom
     */
    public User getUtilisateur(String pseudo) throws UtilisateurInvalideException{
        try{
            PreparedStatement ps = connexion.prepareStatement("SELECT * FROM UTILISATEUR WHERE pseudout = ?");
            ps.setString(1, pseudo);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                String img;
                if (rs.getObject(6) != null){
                    img = "/img/" + rs.getInt(1) + pseudo + ".png";
                }
                else{ img = "/img/null.png"; }
                return new User(rs.getInt("idut"), rs.getString("pseudout"), rs.getString("emailut"), rs.getString("mdput"),rs.getInt("idrole"),img);
            }
            else{
                throw new UtilisateurInvalideException();
            }
        } 
        catch(SQLException e){
            return null;
        }
    }

    /**
     * Ajoute un nouvel utilisateur dans la base de données
     * @param pseudo pseudo de l'utilisateur
     * @param email email de l'utilisateur
     * @param mdp mot de passe de l'utilisateur
     * @return un nouvel utilisateur
     * @throws FileNotFoundException si le fichier n'est pas trouvée
     */
    public User ajouteUser(String pseudo, String email, String mdp)throws FileNotFoundException{
        try{
            ResultSet rs = connexion.prepareStatement("SELECT MAX(idut) FROM UTILISATEUR").executeQuery();
            rs.next();
            int id =  rs.getInt(1)+1;
            PreparedStatement ps = connexion.prepareStatement("INSERT INTO UTILISATEUR(idut,pseudout,emailut,mdput,photo,activeut,idrole) VALUES(?,?,?,?,?,'O',2)");
            ps.setInt(1, id);
            ps.setString(2, pseudo);
            ps.setString(3, email);
            ps.setString(4, mdp);
            FileInputStream file= new FileInputStream(new File("./img/null.png"));
            ps.setBlob(5, file);
            ps.executeUpdate();
            return new User(rs.getInt("idut"), rs.getString("pseudout"), rs.getString("emailut"), rs.getString("mdput"),rs.getInt("idrole"),null);
        }
        catch(SQLException e){
            System.out.println(e);
        }
        return null;
    }

    /**
     *  Permet de mettre a jour le mot de passe d'un utilisateur dans la base de données
     * @param idUt l'id de l'utilisateur
     * @param mdp le nouveau mot de passe
     * @throws UtilisateurInvalideException si l'utilisateur n'existe pas
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public void updateMotDePasse(int idUt, String mdp) throws UtilisateurInvalideException, SQLException{
        try{
            PreparedStatement checkUser = connexion.prepareStatement("select idut from UTILISATEUR  where idut = ?");
            checkUser.setInt(1, idUt);
            ResultSet rs = checkUser.executeQuery();
            if (!rs.next()){
                throw new UtilisateurInvalideException();
            }
            PreparedStatement ps = connexion.prepareStatement("update UTILISATEUR set mdput = ? where idut = ?");
            ps.setString(1,mdp);
            ps.setInt(2,idUt);
            ps.executeUpdate();
        }
        catch (SQLException e){
            System.out.println(e);
        }
    }

    /**
     * Permet de mettre a jour le mail d'un utilisateur dans la base de données
     * @param idUt id de l'utilisateur
     * @param mail nouveau mail de l'utilisateur
     * @throws UtilisateurInvalideException Si l'utilisateur n'existe pas
     * @throws SQLException si il y'a une erreur pendant les requetes sql
     */
    public void updateMail(int idUt, String mail) throws UtilisateurInvalideException, SQLException{
        PreparedStatement checkUser = connexion.prepareStatement("select idut from UTILISATEUR where idut = ?");
        checkUser.setInt(1, idUt);
        ResultSet rs = checkUser.executeQuery();
        if (!rs.next()){
            throw new UtilisateurInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("update UTILISATEUR set emailut = ? where idut = ?");
        ps.setString(1, mail);
        ps.setInt(2, idUt);
        ps.executeUpdate();
    }

    /**
     * Permet de mettre a jour le pseudo d'un utilisateur dans la base de données
     * @param idUt id de l'utilisateur
     * @param pseudo nouveau pseudo de l'utilisateur
     * @throws UtilisateurInvalideException si l'utilisateur n'existe pas
     * @throws PseudoException si le pseudo est déjà utilisé
     * @throws SQLException si il y'a une erreur pendant les requetes sql
     */
    public void updatePseudo(int idUt, String pseudo) throws UtilisateurInvalideException,PseudoException, SQLException{
        PreparedStatement checkUser = connexion.prepareStatement("select idut from UTILISATEUR where idut = ?");
        checkUser.setInt(1, idUt);
        ResultSet rs = checkUser.executeQuery();
        if (!rs.next()){
            throw new UtilisateurInvalideException();
        }
        PreparedStatement checkPseudo = connexion.prepareStatement("select pseudout from UTILISATEUR where pseudout = ?");
        checkPseudo.setString(1,pseudo);
        rs = checkPseudo.executeQuery();
        if (rs.next()){
            throw new PseudoException();
        }
        PreparedStatement ps = connexion.prepareStatement("update UTILISATEUR set pseudout = ? where idut = ?");
        ps.setString(1, pseudo);
        ps.setInt(2, idUt);
        ps.executeUpdate();
    }

    /**
     * met à jour la photo de profil de l'utilisateur dans la base de données
     * @param idut l'id de l'utilisateur
     * @param photoProfil la nouvelle photo de profil
     * @throws UtilisateurInvalideException si l'utilisateur n'existe pas
     * @throws SQLException si erreur lors de la requete sql
     * @throws FileNotFoundException si le fichier n'est pas trouvé
     */
    public void updatePhotoProfil(int idut, byte[] photoProfil) throws UtilisateurInvalideException, SQLException, FileNotFoundException{
        if (photoProfil == null){
            throw new FileNotFoundException();
        }
        PreparedStatement checkUser = connexion.prepareStatement("select idut from UTILISATEUR  where idut = ?");
        checkUser.setInt(1, idut);
        ResultSet rs = checkUser.executeQuery();
        if (!rs.next()){
            throw new UtilisateurInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("update UTILISATEUR set photo = ? where idut = ?");
        ps.setBytes(1, photoProfil);
        ps.setInt(2, idut);
        ps.executeUpdate();
    }

    /**
     * Permet de créer une photo profil
     * @param idut id de l'utilisateur
     * @param pseudo pseudo de l'utilisateur
     * @throws SQLException si il y'a une erreur lors de la requete SQL
     * @throws UtilisateurInvalideException si l'utilisateur n'existe pas
     * @throws IOException si il y'a une erreur lors de l'écriture du nouveau fichier
     * @throws PhotoManquanteException si l'utilisateur n'a pas encore de photo de profil
     */
    public void  getPhoto(int idut, String pseudo) throws SQLException, UtilisateurInvalideException, IOException, PhotoManquanteException{
        PreparedStatement checkUser = connexion.prepareStatement("select idut from UTILISATEUR  where idut = ?");
        checkUser.setInt(1, idut);
        ResultSet rs = checkUser.executeQuery();
        if (!rs.next()){
            throw new UtilisateurInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("select photo from UTILISATEUR where idut = ?");
        ps.setInt(1, idut);
        rs = ps.executeQuery();
        if (rs.next()){
            if (rs.getObject("photo") != null ){
                byte[] photoBytes = rs.getBytes("photo");
                try (OutputStream outputStream = new FileOutputStream("./img/" + idut + pseudo + ".png")) {
                    outputStream.write(photoBytes); 
                }
            }
            else{
                throw new PhotoManquanteException();
            }
        }
        else{
            throw new PhotoManquanteException();
        }
    }

    /**
     *  Retourne true si la sneaker passée en paramètre est dans les favoris de l'utilisateur
     * @param idUt l'id de l'utilisateur
     * @param idVente la confirmation du nouveau mot de passe
     * @throws UtilisateurInvalideException si l'utilisateur n'existe pas
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public boolean sneakerBienEnFavori(int idUt, int idVente) throws UtilisateurInvalideException, SQLException{
        PreparedStatement checkUser = connexion.prepareStatement("select idut from UTILISATEUR  where idut = ?");
        checkUser.setInt(1, idUt);
        ResultSet rs = checkUser.executeQuery();
        if (!rs.next()){
            throw new UtilisateurInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("select * from FAVORI where idut = ? and idsneaker = ?");
        ps.setInt(1,idUt);
        ps.setInt(2, idVente);
        rs = ps.executeQuery();
        if (rs.next()){
            return true;
        }
        return false;
    }

    /**
     * Renvoie la liste des pseudos des utilisateurs
     * @return List<String> : la liste des pseudos des utilisateurs
     */
    public List<String> getLesPseudosUtilisateurs(){
        try{
            List<String> res = new ArrayList<>();
            PreparedStatement ps = connexion.prepareStatement("SELECT pseudout FROM UTILISATEUR");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                res.add(rs.getString(1));
            }
            return res;
        } 
        catch(SQLException e){
            return null;
        }
    }
}

