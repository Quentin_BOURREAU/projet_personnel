import javafx.event.EventHandler;
import javafx.event.ActionEvent ;

/**
 * Controleur d'annuler les modifications d'informations personnelles
 */
public class ControleurAnnuler implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet : vue  de l'appli
    */
    public ControleurAnnuler(AppliProjet appli){
        this.appli = appli;
    }

    /**
     * Actions à effectuer lors du clic sur la croix rouge (annuler les modifications d'informations personnelles)
     *
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        this.appli.majmodeProfilNonEditable();
    }
}