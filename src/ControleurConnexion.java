import javafx.event.EventHandler;

import java.sql.SQLException;
import java.util.Map;
import javafx.scene.Node;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * Controleur de connexion
 */
public class ControleurConnexion implements EventHandler<ActionEvent> {

    /**
     * vue de l'appli
     */
    private AppliProjet appli;
    private ConnexionUserSQL connexion;

    /**
     * @param AppliProjet vue de l'appli
     */
    public ControleurConnexion(AppliProjet appli) {
        this.appli = appli;
        this.connexion = new ConnexionUserSQL();
    }

    /**
     * Savoir si le mot de passe passé en paramètre est valide (respecte les caractéristiques d'un vrai mot de passe)
     * @param mdp : le mot de passe à tester
     * @return true si le mot de passe est valide, sinon false
     */
    public boolean mdpValide(String mdp){
        // vérifier les conditions de validation expliquées dans la pop-up
        if (mdp.length() < 8 || !mdp.matches(".*[A-Z].*") || !mdp.matches(".*[a-z].*") || !mdp.matches(".*\\d.*") || !mdp.matches(".*[@#$%^&+=].*")) {
            return false;
        }
        // le mot de passe satisfait toutes les conditions de validation
        return true;
    }

    /**
     * Actions à effectuer lors du clic sur le bouton connexion
     * 
     * @param event l'événement
     */
    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) (event.getSource());
        if (button.getText().equals("J'ai déjà un compte")) {
            this.appli.modeConnexion();
        }
        else if (button.getText().equals("Valider")) {// procédure de réinitialisation du mot de passe
            UserSQL userSQL = new UserSQL();
            Map<String, Node> lesElementsGraphiques = this.appli.getLesElementsGraphiques();
            TextField tf1 = (TextField) lesElementsGraphiques.get("tfNomUtilisateur");
            TextField tf2 = (TextField) lesElementsGraphiques.get("tfMail");
            PasswordField psf1 = (PasswordField) lesElementsGraphiques.get("passwdf");
            PasswordField psf2 = (PasswordField) lesElementsGraphiques.get("passwdConfirmation");
            try {
                User utilisateur = userSQL.getUtilisateur(tf1.getText());
                if (utilisateur.getEmail().equals(tf2.getText())) { // informations données valides (mail et pseudo)
                    if (!psf1.getText().equals("") && !psf2.getText().equals("") && psf1.getText().equals(psf2.getText())) { // mot de passe et confirmation identiques et pas vide
                        if (!psf1.getText().equals(utilisateur.getPassword())){ // nouveau mdp différent de l'ancien mdp
                            if (this.mdpValide(psf1.getText())){ // nouveau mdp valide (respecte les caractéristiques d'un mdp)
                                try {
                                    userSQL.updateMotDePasse(utilisateur.getIdUser(), psf1.getText());// change le mot de passe sur la base de données
                                }
                                catch (SQLException e) {
                                    System.out.println(e);
                                }
                                this.appli.modeConnexion();
                            }
                            else{ // nouveau mdp invalide (ne respecte pas les caractéristiques d'un mdp)
                                this.appli.popUpPersonnaliseeErreur("Votre mot de passe entré est invalide\n\nUn mot de passe doit avoir :\n-> une longueur minimale de 8 caractères\n\n-> au moins une lettre majuscule\n\n-> au moins une lettre minuscule\n\n-> au moins un chiffre\n\n-> au moins un caractère spécial '@#$%^&+='", "Procédure stoppée", "Mot de passe invalide").showAndWait();
                            }
                        }
                        else{ // mdp identique à l'ancien
                            this.appli.popUpPersonnaliseeErreur("Vous ne pouvez pas remettre un mot de passe déjà utilisé !", "Procédure stoppée", "Mot de passe déjà utilisé").showAndWait();
                        }
                    }
                    else {
                        this.appli.popUpPersonnaliseeErreur("Mots de passes entrés non-correspondants !", "Procédure stoppée", "Mots de passes non-correpondants").showAndWait();
                    }
                }
                else {
                    this.appli.popUpPersonnaliseeErreur("Nom d'utilisateur ou e-mail incorrect !", "Procédure stoppée", "Identifiants saisis incorrects").showAndWait();
                }
            }
            catch (UtilisateurInvalideException e) {
                this.appli.popUpPersonnaliseeErreur("Nom d'utilisateur inconnu !", "Procédure stoppée", "Nom d'utilisateur saisi inconnu").showAndWait();
            }
        }
        else if (button.getText().equals("Annuler la procédure")) { // on souhaite annuler la procédure de réinitialisation de notre mot de passe
            this.appli.modeConnexion();
        }
        else { // on est sur la page Connexion
            Map<String, Node> map = this.appli.getLesElementsGraphiques();
            TextField tf = (TextField) map.get("tfNomUtilisateur");
            PasswordField passwdf = (PasswordField) map.get("passwdf");
            try {
                if (button.getText().contains("Connexion") && this.connexion.userConnexion(tf.getText(), passwdf.getText())) {
                    this.appli.setUtilisateurConnecte(tf.getText());
                    this.appli.modeAccueil();
                }
                else if (tf.getText().equals("") && passwdf.getText().equals("")){ // si aucune information a été entrée
                    this.appli.popUpPersonnaliseeErreur("Aucune information entrée !", "Connexion échouée", "Aucun identifiant entré").showAndWait();
                }
                else{
                    this.appli.popUpPersonnaliseeErreur("Nom d'utilisateur ou mot de passe incorrect !", "Connexion échouée", "Identifiants saisis incorrects").showAndWait();
                }
            }
            catch (MdpUserInvalideException e) {
                this.appli.popUpPersonnaliseeErreur("Nom d'utilisateur ou mot de passe incorrect !", "Connexion échouée", "Identifiants saisis incorrects").showAndWait();
            }
            catch (UtilisateurDesactiveException e){
                this.appli.popUpPersonnaliseeErreur("Votre compte a été désactivé !\nContactez un administrateur", "Compte suspendu", "Votre compte est suspendu").showAndWait();
            }
        }
    }
}