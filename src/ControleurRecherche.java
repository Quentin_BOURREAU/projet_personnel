import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent ;

/**
 * Controleur des recherches
 */
public class ControleurRecherche implements EventHandler<ActionEvent>{
    
    /**
     * vue de l'appli
     */
    private AppliProjet appli;
    /**
     * le modele d'une sneaker
     */
    private SneakerSQL sneakerSQL;

    /**
    * @param AppliProjet  vue de l'appli
    */
    public ControleurRecherche(AppliProjet appli){
        this.appli = appli;
        this.sneakerSQL = new SneakerSQL();
    }

    /**
     * Actions à effectuer lors du clic sur le bouton recherche
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        TextField tf = (TextField) appli.getLesElementsGraphiques().get("tfRechercher");
        this.appli.modeRecherche(this.sneakerSQL.sneakersRecherchees(tf.getText()),tf.getText());
    }
}