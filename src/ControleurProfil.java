import javafx.event.EventHandler;
import javafx.event.ActionEvent ;

/**
 * Controleur du profil
 */
public class ControleurProfil implements EventHandler<ActionEvent>{

    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet : vue  de l'appli
    */
    public ControleurProfil(AppliProjet appli){
        this.appli = appli;
    }
    
    /**
     * Actions à effectuer lors du clic sur le bouton profil
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        this.appli.modeProfil();
    } 
}