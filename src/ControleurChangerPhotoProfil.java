import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import javafx.event.ActionEvent;

/**
 * Controleur de changement de photo de profil 
 */
public class ControleurChangerPhotoProfil implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet : vue de l'appli
    */
    public ControleurChangerPhotoProfil(AppliProjet appli){
        this.appli = appli;
    }
     
    /**
     * Actions à effectuer lors du clic sur le bouton de changement de photo de profil 
     * @param event l'événement
     */    
    @Override
    public void handle(ActionEvent event){
        UserSQL userSQL = new UserSQL();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choisissez une image");
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("*.gif", "*.png");
        fileChooser.getExtensionFilters().add(imageFilter);
        File file = fileChooser.showOpenDialog(null);
        try{
            if (file != null){
                userSQL.updatePhotoProfil(this.appli.getUtilisateurConnecte().getIdUser(), Files.readAllBytes(file.toPath()));
                userSQL.getPhoto(this.appli.getUtilisateurConnecte().getIdUser(), this.appli.getUtilisateurConnecte().getPseudo());
                this.appli.getUtilisateurConnecte().setPhotoProfil("/img/" + this.appli.getUtilisateurConnecte().getIdUser() + this.appli.getUtilisateurConnecte().getPseudo() + ".png");
                this.appli.modeProfil();
            }
        }
        catch (SQLSyntaxErrorException e){ // si l'image est trop grande (en Ko ou Mo)
            this.appli.popUpImageTropGrande().showAndWait();
        }
        catch (PhotoManquanteException e){System.out.println(e);}
        catch (UtilisateurInvalideException e1){System.out.println(e1);}
        catch (FileNotFoundException e2){System.out.println(e2);}
        catch (SQLException e3){System.out.println(e3);} 
        catch (IOException e4){System.out.println(e4);}
    }
} 