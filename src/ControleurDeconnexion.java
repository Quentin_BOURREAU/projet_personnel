import javafx.event.EventHandler;
import java.util.Optional;
import javafx.event.ActionEvent ;
import javafx.scene.control.ButtonType;

/**
 * Controleur de deconnexion
 */
public class ControleurDeconnexion implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet vue de  l'appli
    */
    public ControleurDeconnexion(AppliProjet appli){
        this.appli = appli;
    }

    /**
     * Actions à effectuer lors du clic sur le bouton deconnexion
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        Optional<ButtonType> reponse = this.appli.popUpConnecte().showAndWait(); // on lance la fenêtre popup et on attends la réponse
        // si la réponse est oui
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            this.appli.modeConnexion();
        }
    }
}