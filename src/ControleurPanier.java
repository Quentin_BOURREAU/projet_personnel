import javafx.event.EventHandler;
import javafx.event.ActionEvent ;

/**
 * Controleur du panier
 */
public class ControleurPanier implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet  vue  de l'appli
    */
    public ControleurPanier(AppliProjet appli){
        this.appli = appli;
    }
    
    /**
     * Actions à effectuer lors du clic sur le bouton panier
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        this.appli.modePanier();
    } 
}