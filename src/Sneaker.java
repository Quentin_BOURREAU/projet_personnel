import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Une Sneaker */
public class Sneaker{
    /**id de la sneaker */
    private int idSneaker;
    /**nom de la sneaker */
    private String nomSneaker;
    /**prix de la sneaker */
    private int prix;
    /**id de la marque de la sneaker */
    private int idMarque;
    /**id du sexe de la sneaker */
    private int idSexe;
    /**id du coloris de la sneaker */
    private int idColoris;
    /**la liste des descriptions de la sneaker */
    private List<String> lesDescriptionsSneaker;
    /**la liste d'images de la sneaker */
    private List<String> lesImagesSneaker;
    /**le dictionnaire avec comme clé une taille et en valeur le nombre de stocks qu'il y a de cette taille pour la sneaker */
    private Map<Integer, Integer> lesStocks;

    /**
     * Constructeur d'une sneaker
     * @param idSneaker l'id de la sneaker
     * @param nomSneaker le nom de la sneaker
     * @param prix le prix de la sneaker
     * @param idMarque l'id de la marque de la sneaker
     * @param idSexe l'id du sexe de la sneaker
     * @param idColoris l'id du coloris de la sneaker
     */
    public Sneaker(int idSneaker, String nomSneaker, int prix, int idMarque, int idSexe, int idColoris){
        this.idSneaker = idSneaker;
        this.nomSneaker = nomSneaker;
        this.prix = prix;
        this.idMarque = idMarque;
        this.idSexe = idSexe;
        this.idColoris = idColoris;
        this.lesDescriptionsSneaker = new ArrayList<>();
        this.lesImagesSneaker = new ArrayList<>();
        this.lesStocks = new HashMap<Integer, Integer>();
    }

    /**
     * Retourne l'id de la sneaker
     * @return int : l'id de la sneaker
     */
    public int getIdSneaker(){
        return this.idSneaker;
    }

    /**
     * Retourne le nom de la sneaker
     * @return String : le nom de la sneaker
     */
    public String getNomSneaker(){
        return this.nomSneaker;
    }

    /**
     * Retourne la description de la sneaker de l'indice choisi
     * @param indiceDescription : l'indice de la description choisie
     * @return String : la description de la sneaker de l'indice choisi
     */
    public String getDescriptionSneaker(int indiceDescription){
        return this.lesDescriptionsSneaker.get(indiceDescription);
    }

    /**
     * Retourne le prix de la sneaker
     * @return int : le prix de la sneaker
     */
    public int getPrix(){
        return this.prix;
    }

    /**
     * Retourne l'id de la marque de la sneaker
     * @return int : l'id de la marque de la sneaker
     */
    public int getIdMarque(){
        return this.idMarque;
    }

    /**
     * Retourne l'id du sexe de la sneaker
     * @return int : l'id du sexe de la sneaker
     */
    public int getIdSexe(){
        return this.idSexe;
    }

    /**
     * Retourne l'id du coloris de la sneaker
     * @return int : l'id du coloris de la sneaker
     */
    public int getIdColoris(){
        return this.idColoris;
    }

    /**
     * Retourne l'image demandée de la sneaker
     * @param indicePhoto : l'indice de la photo demandée
     * @return String : l'image demandée de la sneaker
     */
    public String getImageSneaker(int indicePhoto){
        if (this.lesImagesSneaker.isEmpty()){ // s'il n'y a encore aucune image pour la sneaker
            return "/img/default.png";
        }
        return this.lesImagesSneaker.get(indicePhoto);
    }

    /**
     * Retourne les stocks disponibles suivant chaque taille
     * @return Map<Integer, Integer> : le dictionnaire des stocks pour la sneaker
     */
    public Map<Integer, Integer> getStocks(){
        return this.lesStocks;
    }

    /**
     * Ajoute une image à la liste d'images de la sneaker
     * @param cheminNouvellePhoto : le chemin de la nouvelle image
     */
    public void ajouteImage(String cheminNouvellePhoto) {
        this.lesImagesSneaker.add(cheminNouvellePhoto);
    }

    /**
     * Ajoute une description à la liste de descriptions de la sneaker
     * @param nouvelleDescription : la nouvelle description à ajouter
     */
    public void ajouteDescription(String nouvelleDescription) {
        this.lesDescriptionsSneaker.add(nouvelleDescription);
    }

    /**
     * Ajoute le nombre de stocks disponibles suivant les tailles disponibles et remplis sinon à 0
     */
    public void rempliStocks(List<Integer> lesTailles, Map<Integer, Integer> lesTaillesDisponibles){
        for (Integer tailleActuelle : lesTailles){
            if (lesTaillesDisponibles.containsKey(tailleActuelle) && lesTaillesDisponibles.get(tailleActuelle) > 0){ // s'il y a au moins 1 stock pour cette taille
                this.lesStocks.put(tailleActuelle, lesTaillesDisponibles.get(tailleActuelle));
            }
            else{
                this.lesStocks.put(tailleActuelle, 0);
            }
        }
    }

    /**
     * Réinitialise la liste d'images de la sneaker
     */
    public void resetLesImages(){
        this.lesImagesSneaker = new ArrayList<>();
    }

    /**
     * Réinitialise la liste de descriptions de la sneaker
     */
    public void resetLesDescriptions(){
        this.lesDescriptionsSneaker = new ArrayList<>();
    }
}