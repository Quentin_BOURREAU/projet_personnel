import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class ControleurMotDePasseOublie implements EventHandler<MouseEvent>{

    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet vue  de l'appli
    */
    public ControleurMotDePasseOublie(AppliProjet appli){
        this.appli = appli;
    }

    /**
     * Actions à effectuer lors du clic sur le texte "Mot de passe oublié ?"
     * @param event l'événement
     */     
    @Override
    public void handle(MouseEvent event){
        this.appli.modeMotDePasseOublie();
    }
}
