import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * Vue de l'appli projet
 */
public class AppliProjet extends Application {

    /**
     * le panel Central qui pourra être modifié selon la fenetre voulue
     */
    private BorderPane panelCentral;
    /**
     * la fenetre (root) de l'appli
     */
    private BorderPane fenetre;
    /**
     * la hashMap des boutons et des textFields avec comme clé le nom du Node
     * (String) et en valeurs le Node
     */
    private Map<String, Node> lesElementsGraphiques;
    /**
     * l'utilisateur connecté
     */
    private User utilisateurConnecte;
    /**
     * le modele pour les utilisateurs
     */
    private UserSQL userSQL;
    /**
     * le modele pour les sneakers
     */
    private SneakerSQL sneakerSQL;

    /**
     * initialise les attributs
     */
    @Override
    public void init() {
        this.panelCentral = new BorderPane();
        this.fenetre = new BorderPane();
        this.lesElementsGraphiques = new HashMap<>();
        this.utilisateurConnecte = null;
        this.userSQL = new UserSQL();
        this.sneakerSQL = new SneakerSQL();
    }

    /**
     * @return le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene() {
        this.fenetre.setTop(this.titreConnexionInscription());
        this.fenetre.setCenter(this.panelCentral);
        return new Scene(this.fenetre, 1920, 1080);
    }

    /**
     * @return le panel contenant le titre pour la page Connexion et Inscription
     */
    private Pane titreConnexionInscription() {
        BorderPane banniere = new BorderPane();
        banniere.setStyle("-fx-background-color: #3B3939");
        return banniere;
    }

    /**
     * @return le panel contenant le titre pour les autres pages
     */
    private Pane titreAutresPages() {
        BorderPane banniere = new BorderPane();
        banniere.setBackground(this.getBackgroundAutresPages());

        // Image du logo (left) du borderPane
        Button boutonLogo = new Button();
        boutonLogo.setOnAction(new ControleurHome(this));
        // boutonLogo.setStyle("-fx-background-color: transparent");
        this.creeHoverBouton(boutonLogo, "#8787DE", "#5C5C96");
        ImageView imageLogo = new ImageView(new Image("file:./img/logo.png"));
        imageLogo.setFitWidth(200);
        imageLogo.setFitHeight(70);
        boutonLogo.setGraphic(imageLogo);
        BorderPane.setMargin(boutonLogo, new Insets(20, 50, 20, 30));
        banniere.setLeft(boutonLogo);

        // TextField (center) du borderPane
        TextField tfRechercher = new TextField("");
        this.lesElementsGraphiques.put("tfRechercher", tfRechercher);
        tfRechercher.setFont(this.getPolicePersonnalisee(15));
        tfRechercher.setStyle("-fx-background-color: #8787DE; -fx-background-radius: 10; -fx-prompt-text-fill: black");
        tfRechercher.setPromptText("Rechercher par nom de sneaker...");
        tfRechercher.setMaxWidth(700);
        tfRechercher.setOnAction(new ControleurRecherche(this));
        banniere.setCenter(tfRechercher);
        BorderPane.setAlignment(tfRechercher, Pos.CENTER);

        // HBox (right) du borderPane)
        HBox hBox = new HBox(20);
        hBox.setAlignment(Pos.CENTER_RIGHT);
        Button boutonDeconnexion = new Button("Déconnexion");
        boutonDeconnexion.setOnAction(new ControleurDeconnexion(this));
        boutonDeconnexion.setPrefWidth(200);
        boutonDeconnexion.setPrefHeight(50);
        this.creeHoverBouton(boutonDeconnexion, "#F1C22B", "#B79623");
        boutonDeconnexion.setTextFill(Color.BLACK);
        boutonDeconnexion.setFont(this.getPolicePersonnalisee(25));
        int nombreSneakersFavoris = this.sneakerSQL.sneakersFavori(this.utilisateurConnecte.getPseudo()).size(); // récupère
                                                                                                                 // le
                                                                                                                 // nombre
                                                                                                                 // de
                                                                                                                 // sneakers
                                                                                                                 // en
                                                                                                                 // favori
                                                                                                                 // de
                                                                                                                 // l'utilisateur
                                                                                                                 // connecté
        Button boutonFavoris = new Button();
        this.lesElementsGraphiques.put("boutonFavoris", boutonFavoris);
        if (nombreSneakersFavoris > 0) { // si l'utilisateur connecté à au moins 1 sneaker en favori
            boutonFavoris.setText(nombreSneakersFavoris + "");
        }
        boutonFavoris.setContentDisplay(ContentDisplay.CENTER);
        boutonFavoris.setOnAction(new ControleurFavori(this));
        this.creeHoverBouton(boutonFavoris, "#DFDFDF", "#7F7F7F");
        ImageView imageLikes = new ImageView(new Image("file:./img/like.png"));
        imageLikes.setFitWidth(45);
        imageLikes.setFitHeight(40);
        boutonFavoris.setGraphic(imageLikes);
        Button boutonHome = new Button();
        boutonHome.setOnAction(new ControleurHome(this));
        this.creeHoverBouton(boutonHome, "#DFDFDF", "#7F7F7F");
        ImageView imageHome = new ImageView(new Image("file:./img/home.png"));
        imageHome.setFitWidth(45);
        imageHome.setFitHeight(40);
        boutonHome.setGraphic(imageHome);
        Map<List<Integer>, List<Sneaker>> lesSneakersPanier = this.sneakerSQL
                .sneakersPanier(this.utilisateurConnecte.getPseudo());
        Collection<List<Sneaker>> collectionLesSneakers = lesSneakersPanier.values();
        List<Sneaker> lesSneakers = new ArrayList<>();
        for (List<Sneaker> liste : collectionLesSneakers) {
            lesSneakers = liste;
        }
        int nombreSneakersPanier = lesSneakers.size();
        Button boutonPanier = new Button();
        this.lesElementsGraphiques.put("boutonPanier", boutonPanier);
        if (nombreSneakersPanier > 0) { // si l'utilisateur connecté à au moins 1 sneaker au panier
            boutonPanier.setText(nombreSneakersPanier + "");
        }
        boutonPanier.setContentDisplay(ContentDisplay.CENTER);
        boutonPanier.setOnAction(new ControleurPanier(this));
        this.creeHoverBouton(boutonPanier, "#DFDFDF", "#7F7F7F");
        ImageView imagePanier = new ImageView(new Image("file:./img/panier.png"));
        imagePanier.setFitWidth(45);
        imagePanier.setFitHeight(40);
        boutonPanier.setGraphic(imagePanier);
        Button boutonProfil = new Button();
        boutonProfil.setOnAction(new ControleurProfil(this));
        this.creeHoverBouton(boutonProfil, "#DFDFDF", "#7F7F7F");
        ImageView imageProfil = new ImageView(new Image("file:./img/profil.png"));
        imageProfil.setFitWidth(45);
        imageProfil.setFitHeight(40);
        boutonProfil.setGraphic(imageProfil);
        HBox.setMargin(boutonDeconnexion, new Insets(20, 30, 20, 0));
        HBox.setMargin(boutonHome, new Insets(20, 0, 20, 0));
        HBox.setMargin(boutonFavoris, new Insets(20, 0, 20, 0));
        HBox.setMargin(boutonPanier, new Insets(20, 0, 20, 0));
        HBox.setMargin(boutonProfil, new Insets(20, 30, 20, 0));
        hBox.getChildren().addAll(boutonDeconnexion, boutonFavoris, boutonHome, boutonPanier, boutonProfil);
        banniere.setRight(hBox);

        return banniere;
    }

    /**
     * @return Pane : le panel contenant le bandeau inférieur pour les autres pages
     */
    public Pane bandeauInferieurAutresPages() {
        BorderPane banniereInferieur = new BorderPane();
        banniereInferieur.setBackground(this.getBackgroundAutresPages());

        VBox vboxBandeauInferieur = new VBox(20);
        vboxBandeauInferieur.setAlignment(Pos.CENTER);
        Text textTest = new Text("Copyright - droits d'auteur : Quentin BOURREAU");
        textTest.setFont(this.getPolicePersonnalisee(30));
        Separator separator = new Separator();
        separator.setStyle("-fx-background-color: black");
        separator.setPrefHeight(2); // épaisseur du trait en pixels
        separator.setPrefWidth(1820); // définition de la largeur préférée du Separator
        vboxBandeauInferieur.getChildren().addAll(textTest, separator);
        banniereInferieur.setCenter(vboxBandeauInferieur);

        return banniereInferieur;
    }

    /**
     * change la fenetre pour la page accueil
     */
    public void modeAccueil() {
        // réinitialisation des éléments
        this.panelCentral = new BorderPane();
        ScrollPane scrollPaneAccueil = new ScrollPane(this.panelCentral);
        scrollPaneAccueil.setFitToWidth(true); // enlève la bordure droite du ScrollPane de base
        this.panelCentral.setBackground(this.getBackgroundAutresPages());

        // partie Filtres (left du BorderPane)
        VBox vboxFiltres = this.creePartieFiltres();
        BorderPane.setMargin(vboxFiltres, new Insets(70, 30, 30, 15));
        this.panelCentral.setLeft(vboxFiltres);

        // partie accueil avec Dernière sortie (center du BorderPane)
        HBox hboxDerniereSortie = new HBox(40);
        hboxDerniereSortie.setAlignment(Pos.TOP_CENTER);
        ImageView imageDerniereSortie = this.creeImageSneaker("jordan4seafoam.jpg");
        VBox vboxInfosSneaker = new VBox(5);
        Text textDerniereSortie = new Text("DERNIÈRE SORTIE");
        textDerniereSortie.setFont(this.getPolicePersonnalisee(45));
        // création du séparateur en-dessous du text "Filtres"
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: black");
        separator.setPrefHeight(3); // epaisseur du trait en pixels
        separator.setPrefWidth(400); // définition de la largeur préférée du Separator
        Text textNomSneaker = new Text("Air Jordan 4 Seafoam");
        textNomSneaker.setFont(this.getPolicePersonnalisee(40));
        Text textInfosSneaker = new Text(
                "La Air Jordan 4 Seafoam affiche une empeigne en cuir blanc accompagnée de détails noirs comme les rabats et...");
        textInfosSneaker.setWrappingWidth(400);
        textInfosSneaker.setFont(this.getPolicePersonnalisee(20));
        // création du bouton "En savoir plus"
        Button boutonEnSavoirPlus = this.creeBoutonEnSavoirPlus("1");
        vboxInfosSneaker.getChildren().addAll(textDerniereSortie, separator, textNomSneaker, textInfosSneaker,
                boutonEnSavoirPlus);
        hboxDerniereSortie.getChildren().addAll(imageDerniereSortie, vboxInfosSneaker);
        VBox.setMargin(textNomSneaker, new Insets(30, 0, 30, 0));
        VBox.setMargin(textInfosSneaker, new Insets(0, 0, 10, 0));
        BorderPane.setMargin(hboxDerniereSortie, new Insets(70, 0, 0, 70));
        this.panelCentral.setCenter(hboxDerniereSortie);

        // partie accueil avec 4 sneakers + Notre sélection + Un peu d'extravagence
        // (bottom du BorderPane)
        VBox vboxPartieBottomAccueil = new VBox(80);
        // 4 sneakers (GridPane)
        GridPane gridPane4Sneakers = this.creePartie4Sneakers();

        // Notre sélection
        VBox vboxNotreSelection = this.creePartieNotreSelection();

        // Un peu d'extravagence
        HBox hboxUnPeuExtravagence = this.creePartieUnPeuExtravagence();

        vboxPartieBottomAccueil.getChildren().addAll(gridPane4Sneakers, vboxNotreSelection, hboxUnPeuExtravagence);

        // margins + ajout vbox
        VBox.setMargin(vboxNotreSelection, new Insets(0, 0, 100, 0));
        VBox.setMargin(hboxUnPeuExtravagence, new Insets(0, 0, 100, 0));
        this.panelCentral.setBottom(vboxPartieBottomAccueil);

        this.fenetre.setTop(this.titreAutresPages());
        this.fenetre.setCenter(scrollPaneAccueil);
    }

    /**
     * Créé une imageView de la sneaker passée en paramètre (nom du fichier)
     * 
     * @param nomSneakerFichier : le nom du fichier de la sneaker
     * @return ImageView : l'image de la sneaker à afficher
     */
    public ImageView creeImageSneaker(String nomSneakerFichier) {
        ImageView imageSneaker = new ImageView(new Image("file:./img/" + nomSneakerFichier));
        imageSneaker.setFitHeight(370);
        imageSneaker.setFitWidth(600);
        return imageSneaker;
    }

    /**
     * Créé le bouton En savoir plus (pour la page Accueil)
     * 
     * @param idBouton : l'id du bouton à créer (pour le reconnaitres après dans les
     *                 controleurs)
     * @return Bouton : le bouton a afficher sur la vue
     */
    public Button creeBoutonEnSavoirPlus(String idBouton) {
        Button boutonEnSavoirPlus = new Button("En savoir plus");
        boutonEnSavoirPlus.setId(idBouton);
        boutonEnSavoirPlus.setPrefWidth(200);
        boutonEnSavoirPlus.setPrefHeight(50);
        boutonEnSavoirPlus.setOnAction(new ControleurEnSavoirPlus(this));
        this.creeHoverBouton(boutonEnSavoirPlus, "#F1C22B", "#B79623");
        boutonEnSavoirPlus.setTextFill(Color.BLACK);
        boutonEnSavoirPlus.setFont(this.getPolicePersonnalisee(25));
        return boutonEnSavoirPlus;
    }

    /**
     * Créé la partie Filtres (gauche de chaque page autre que Connexion et
     * Inscription)
     * 
     * @return VBox : la vbox de la partie Filtres
     */
    public VBox creePartieFiltres() {
        VBox vboxFiltres = new VBox();
        vboxFiltres.setAlignment(Pos.TOP_CENTER);
        vboxFiltres.setStyle("-fx-background-color: #8787DE; -fx-background-radius: 10");
        Text textFiltres = new Text("Filtres");
        textFiltres.setFill(Color.WHITE);
        textFiltres.setFont(this.getPolicePersonnalisee(55));
        // création du séparateur en-dessous du text "Filtres"
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: white");
        separator.setPrefHeight(3); // epaisseur du trait en pixels
        separator.setPrefWidth(300); // définition de la largeur préférée du Separator

        // filtre par Marque
        VBox vboxCheckBoxs1 = new VBox(10);
        TitledPane titledPaneMarque = new TitledPane("MARQUE", vboxCheckBoxs1);
        titledPaneMarque.setExpanded(false); // définit le TitledPane comme fermé par défaut
        titledPaneMarque.setFont(this.getPolicePersonnalisee(20));
        CheckBox cbNike = new CheckBox("Nike");
        cbNike.setFont(this.getPolicePersonnalisee(20));
        CheckBox cbJordan = new CheckBox("Jordan");
        cbJordan.setFont(this.getPolicePersonnalisee(20));
        CheckBox cbAdidas = new CheckBox("Adidas");
        cbAdidas.setFont(this.getPolicePersonnalisee(20));
        CheckBox cbNB = new CheckBox("New Balance");
        cbNB.setFont(this.getPolicePersonnalisee(20));
        CheckBox cbConverse = new CheckBox("Converse");
        cbConverse.setFont(this.getPolicePersonnalisee(20));
        vboxCheckBoxs1.getChildren().addAll(cbNike, cbJordan, cbAdidas, cbNB, cbConverse);

        // filtre par Sexe
        VBox vboxCheckBoxs2 = new VBox(10);
        TitledPane titledPaneSexe = new TitledPane("SEXE", vboxCheckBoxs2);
        titledPaneSexe.setExpanded(false); // définit le TitledPane comme fermé par défaut
        titledPaneSexe.setFont(this.getPolicePersonnalisee(20));
        CheckBox cbHommes = new CheckBox("Hommes");
        cbHommes.setFont(this.getPolicePersonnalisee(20));
        CheckBox cbFemmes = new CheckBox("Femmes");
        cbFemmes.setFont(this.getPolicePersonnalisee(20));
        vboxCheckBoxs2.getChildren().addAll(cbHommes, cbFemmes);

        // filtre par Coloris
        VBox vboxCheckBoxs3 = new VBox(10);
        TitledPane titledPaneColoris = new TitledPane("COLORIS", vboxCheckBoxs3);
        titledPaneColoris.setExpanded(false); // définit le TitledPane comme fermé par défaut
        titledPaneColoris.setFont(this.getPolicePersonnalisee(20));
        CheckBox cbBlanc = new CheckBox();
        cbBlanc.setStyle("-fx-background-color: white");
        CheckBox cbNoir = new CheckBox();
        cbNoir.setStyle("-fx-background-color: black");
        CheckBox cbRouge = new CheckBox();
        cbRouge.setStyle("-fx-background-color: red");
        vboxCheckBoxs3.getChildren().addAll(cbBlanc, cbNoir, cbRouge);

        // filtre par Taille
        GridPane gridPaneCheckBoxs4 = new GridPane();
        gridPaneCheckBoxs4.setGridLinesVisible(true);
        TitledPane titledPaneTaille = new TitledPane("TAILLE", gridPaneCheckBoxs4);
        titledPaneTaille.setExpanded(false); // définit le TitledPane comme fermé par défaut
        titledPaneTaille.setFont(this.getPolicePersonnalisee(20));
        CheckBox cb36 = new CheckBox();
        cb36.setFont(this.getPolicePersonnalisee(20));
        CheckBox cb37 = new CheckBox();
        cb37.setFont(this.getPolicePersonnalisee(20));
        CheckBox cb38 = new CheckBox();
        cb38.setFont(this.getPolicePersonnalisee(20));
        gridPaneCheckBoxs4.add(cb36, 0, 0);
        gridPaneCheckBoxs4.add(cb37, 1, 0);
        gridPaneCheckBoxs4.add(cb38, 2, 0);

        // filtre par Prix
        GridPane gridPanePrix = new GridPane();
        TitledPane titledPanePrix = new TitledPane("PRIX", gridPanePrix);
        titledPanePrix.setExpanded(false); // définit le TitledPane comme fermé par défaut
        titledPanePrix.setFont(this.getPolicePersonnalisee(20));
        gridPanePrix.setAlignment(Pos.CENTER);
        gridPanePrix.setHgap(10); // espacement horizontal entre les colonnes
        gridPanePrix.setVgap(10); // espacement vertical entre les lignes
        Text textPrixMin = new Text("Prix minimum");
        textPrixMin.setFont(this.getPolicePersonnalisee(20));
        gridPanePrix.add(textPrixMin, 0, 0);
        Text textPrixMax = new Text("Prix maximum");
        textPrixMax.setFont(this.getPolicePersonnalisee(20));
        gridPanePrix.add(textPrixMax, 1, 0);
        TextField integerTextFieldMin = new TextField();
        integerTextFieldMin.setMaxWidth(50); // Créé un TextField de largeur fixe
        TextFormatter<Integer> integerFormatter = new TextFormatter<>(change -> { // utilise un TextFormatter pour
                                                                                  // restreindre les caractères acceptés
                                                                                  // aux entiers
            String newText = change.getControlNewText();
            if (newText.matches("\\d*")) { // vérifie si la nouvelle valeur est un nombre entier
                return change; // autorise le changement
            }
            return null; // rejete le changement
        });
        integerTextFieldMin.setTextFormatter(integerFormatter);
        gridPanePrix.add(integerTextFieldMin, 0, 1);
        TextField integerTextFieldMax = new TextField();
        integerTextFieldMax.setMaxWidth(50); // Créé un TextField de largeur fixe
        TextFormatter<Integer> integerFormatter2 = new TextFormatter<>(change -> { // utilise un TextFormatter pour
                                                                                   // restreindre les caractères
                                                                                   // acceptés aux entiers
            String newText = change.getControlNewText();
            if (newText.matches("\\d*")) { // vérifie si la nouvelle valeur est un nombre entier
                return change; // autorise le changement
            }
            return null; // rejete le changement
        });
        integerTextFieldMax.setTextFormatter(integerFormatter2);
        gridPanePrix.add(integerTextFieldMax, 1, 1);
        GridPane.setMargin(integerTextFieldMin, new Insets(0, 0, 0, 40));
        GridPane.setMargin(integerTextFieldMax, new Insets(0, 40, 0, 0));

        // création du bouton "Appliquer"
        Button boutonAppliquer = new Button("Appliquer");
        boutonAppliquer.setPrefWidth(200);
        boutonAppliquer.setPrefHeight(50);
        boutonAppliquer.setOnAction(new ControleurAppliquerFiltres(this));
        this.creeHoverBouton(boutonAppliquer, "#F1C22B", "#B79623");
        boutonAppliquer.setTextFill(Color.BLACK);
        boutonAppliquer.setFont(this.getPolicePersonnalisee(25));

        vboxFiltres.getChildren().addAll(textFiltres, separator, titledPaneMarque, titledPaneSexe, titledPaneColoris,
                titledPaneTaille, titledPanePrix, boutonAppliquer);
        // gestion des marges
        VBox.setMargin(textFiltres, new Insets(30, 0, 0, 0));
        VBox.setMargin(separator, new Insets(5, 30, 60, 30));
        VBox.setMargin(titledPaneMarque, new Insets(0, 30, 0, 30));
        VBox.setMargin(titledPaneSexe, new Insets(0, 30, 0, 30));
        VBox.setMargin(titledPaneColoris, new Insets(0, 30, 0, 30));
        VBox.setMargin(titledPaneTaille, new Insets(0, 30, 0, 30));
        VBox.setMargin(titledPanePrix, new Insets(0, 30, 0, 30));
        VBox.setMargin(boutonAppliquer, new Insets(30, 0, 30, 0));
        return vboxFiltres;
    }

    /**
     * Créé la partie avec les 4 sneakers (page Accueil)
     * 
     * @return GridPane : le gridPane de la partie avec les 4 sneakers (page
     *         Accueil)
     */
    public GridPane creePartie4Sneakers() {
        GridPane gridPaneSneakers = new GridPane();
        gridPaneSneakers.setAlignment(Pos.CENTER);

        // 1ère sneaker (1906r Protection Pack Beige)
        VBox vboxSneaker1 = new VBox(20);
        vboxSneaker1.setAlignment(Pos.TOP_LEFT);
        ImageView imageSneaker1 = this.creeImageSneaker("1906r.jpeg");
        Text textNomSneaker1 = new Text("1906r Protection Pack Beige");
        textNomSneaker1.setFont(this.getPolicePersonnalisee(40));
        Text textInfosSneaker1 = new Text(
                "La New Balance 1906R Protection Pack Beige affiche une base en mesh beige accompagnée d'empiècements...");
        textInfosSneaker1.setFont(this.getPolicePersonnalisee(20));
        textInfosSneaker1.setWrappingWidth(600);
        Button boutonEnSavoirPlusSneaker1 = this.creeBoutonEnSavoirPlus("2");
        vboxSneaker1.getChildren().addAll(imageSneaker1, textNomSneaker1, textInfosSneaker1,
                boutonEnSavoirPlusSneaker1);
        gridPaneSneakers.add(vboxSneaker1, 0, 0);

        // 2ème sneaker (Nike Air More Uptempo Valentine’s Day)
        VBox vboxSneaker2 = new VBox(20);
        vboxSneaker2.setAlignment(Pos.TOP_RIGHT);
        ImageView imageSneaker2 = this.creeImageSneaker("airmoreuptempo.jpg");
        Text textNomSneaker2 = new Text("Nike Air More Uptempo Valentine's Day");
        textNomSneaker2.setFont(this.getPolicePersonnalisee(40));
        Text textInfosSneaker2 = new Text(
                "Le modèle affiche une empeigne en suede marron agrémenté d’un AIR contourné de suede à longs poils rose, matériau qu’on...");
        textInfosSneaker2.setFont(this.getPolicePersonnalisee(20));
        textInfosSneaker2.setWrappingWidth(600);
        Button boutonEnSavoirPlusSneaker2 = this.creeBoutonEnSavoirPlus("3");
        vboxSneaker2.getChildren().addAll(imageSneaker2, textNomSneaker2, textInfosSneaker2,
                boutonEnSavoirPlusSneaker2);
        gridPaneSneakers.add(vboxSneaker2, 1, 0);

        // 3ème sneaker (Jordan 2 Lucky Green)
        VBox vboxSneaker3 = new VBox(20);
        vboxSneaker3.setAlignment(Pos.TOP_LEFT);
        ImageView imageSneaker3 = this.creeImageSneaker("jordan2retrogreen.jpg");
        Text textNomSneaker3 = new Text("Jordan 2 Lucky Green");
        textNomSneaker3.setFont(this.getPolicePersonnalisee(40));
        Text textInfosSneaker3 = new Text(
                "La superposition d’empiècements en cuir lisse blanc et en cuir grainé de la même teinte apporte de la texture...");
        textInfosSneaker3.setFont(this.getPolicePersonnalisee(20));
        textInfosSneaker3.setWrappingWidth(600);
        Button boutonEnSavoirPlusSneaker3 = this.creeBoutonEnSavoirPlus("4");
        vboxSneaker3.getChildren().addAll(imageSneaker3, textNomSneaker3, textInfosSneaker3,
                boutonEnSavoirPlusSneaker3);
        gridPaneSneakers.add(vboxSneaker3, 0, 1);

        // 4ème sneaker (Forum Buckle Low Bad Bunny Blue Tint)
        VBox vboxSneaker4 = new VBox(20);
        vboxSneaker4.setAlignment(Pos.TOP_RIGHT);
        ImageView imageSneaker4 = this.creeImageSneaker("adidasXbadbunny.jpg");
        Text textNomSneaker4 = new Text("Forum Buckle Low Bad Bunny Blue Tint");
        textNomSneaker4.setFont(this.getPolicePersonnalisee(40));
        Text textInfosSneaker4 = new Text(
                "Apparue pour la première fois en 1984, la Forum Low est sans doute l’une des silhouettes les plus rétro d’Adidas...");
        textInfosSneaker4.setFont(this.getPolicePersonnalisee(20));
        textInfosSneaker4.setWrappingWidth(600);
        Button boutonEnSavoirPlusSneaker4 = this.creeBoutonEnSavoirPlus("5");
        vboxSneaker4.getChildren().addAll(imageSneaker4, textNomSneaker4, textInfosSneaker4,
                boutonEnSavoirPlusSneaker4);
        gridPaneSneakers.add(vboxSneaker4, 1, 1);

        // margins
        GridPane.setMargin(vboxSneaker1, new Insets(100, 250, 100, 0));
        GridPane.setMargin(vboxSneaker2, new Insets(100, 0, 100, 0));
        GridPane.setMargin(vboxSneaker3, new Insets(0, 250, 150, 0));
        GridPane.setMargin(vboxSneaker4, new Insets(50, 0, 150, 0));

        return gridPaneSneakers;
    }

    /**
     * Créé la partie "Notre sélection" (page Accueil)
     * 
     * @return VBox : la vbox de la partie "Notre sélection" (page Accueil)
     */
    public VBox creePartieNotreSelection() {
        VBox vboxNotreSelection = new VBox(20);
        vboxNotreSelection.setAlignment(Pos.CENTER);
        vboxNotreSelection.setStyle("-fx-background-color : black");
        Text textNotreSelection = new Text("NOTRE SÉLECTION");
        textNotreSelection.setFont(this.getPolicePersonnalisee(45));
        textNotreSelection.setFill(Color.WHITE);
        // création du séparateur en-dessous du text "NOTRE SÉLECTION"
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: white");
        separator.setPrefHeight(3); // epaisseur du trait en pixels
        separator.setMaxWidth(500); // définition de la largeur préférée du Separator

        // HBox (1ère sneaker)
        HBox hboxSneaker1 = new HBox(90);
        hboxSneaker1.setAlignment(Pos.CENTER);
        VBox vboxInfosSneaker1 = new VBox(20);
        vboxInfosSneaker1.setAlignment(Pos.CENTER);
        Text textNomSneaker1 = new Text("2002R Protection Pack Rain Cloud");
        textNomSneaker1.setFont(this.getPolicePersonnalisee(40));
        textNomSneaker1.setFill(Color.WHITE);
        Text textInfos1Sneaker1 = new Text(
                "Reconnue pour être la plus performante et la plus qualitative de la gamme, la silhouette sortie en 2010 revient fort sur la scène sneakers !");
        textInfos1Sneaker1.setFont(this.getPolicePersonnalisee(20));
        textInfos1Sneaker1.setWrappingWidth(650);
        textInfos1Sneaker1.setFill(Color.WHITE);
        textInfos1Sneaker1.setTextAlignment(TextAlignment.CENTER);
        Separator separator2 = new Separator(); // un séparateur pour l'esthétique uniquement
        separator2.setStyle("-fx-background-color: white");
        separator2.setPrefHeight(3); // epaisseur du trait en pixels
        separator2.setMaxWidth(450); // définition de la largeur préférée du Separator
        Text textInfos2Sneaker1 = new Text(
                "La New Balance 2002R Protection Pack Rain Cloud présente une base qui associe mesh, cuir lisse et suède. L'aspect déconstruit est apporté par l'effet brut des bords de chaque empiècement et de la languette. Une semelle couplant les technologies N-Ergy etABZORB finalise l'ensemble.");
        textInfos2Sneaker1.setFont(this.getPolicePersonnalisee(20));
        textInfos2Sneaker1.setWrappingWidth(650);
        textInfos2Sneaker1.setFill(Color.WHITE);
        textInfos2Sneaker1.setTextAlignment(TextAlignment.CENTER);
        Button boutonEnSavoirPlusSneaker1 = this.creeBoutonEnSavoirPlus("14");
        vboxInfosSneaker1.getChildren().addAll(textNomSneaker1, textInfos1Sneaker1, separator2, textInfos2Sneaker1,
                boutonEnSavoirPlusSneaker1);
        ImageView imageSneaker1 = this.creeImageSneaker("nb2002r.jpg");
        hboxSneaker1.getChildren().addAll(vboxInfosSneaker1, imageSneaker1);

        // HBox (2ème sneaker)
        HBox hboxSneaker2 = new HBox(90);
        hboxSneaker2.setAlignment(Pos.CENTER);
        ImageView imageSneaker2 = this.creeImageSneaker("dunkoffwhiteunc.jpg");
        VBox vboxInfosSneaker2 = new VBox(20);
        vboxInfosSneaker2.setAlignment(Pos.CENTER);
        Text textNomSneaker2 = new Text("Off-White x Futura x Nike Dunk Low UNC");
        textNomSneaker2.setFont(this.getPolicePersonnalisee(40));
        textNomSneaker2.setFill(Color.WHITE);
        Text textInfos1Sneaker2 = new Text(
                "Un peu moins d'un an après sa mort, Nike et Off-White rendent hommage à Virgil Abloh avec cette sortie le jour de son anniversaire !");
        textInfos1Sneaker2.setFont(this.getPolicePersonnalisee(20));
        textInfos1Sneaker2.setWrappingWidth(650);
        textInfos1Sneaker2.setFill(Color.WHITE);
        textInfos1Sneaker2.setTextAlignment(TextAlignment.CENTER);
        Separator separator3 = new Separator(); // un séparateur pour l'esthétique uniquement
        separator3.setStyle("-fx-background-color: white");
        separator3.setPrefHeight(3); // epaisseur du trait en pixels
        separator3.setMaxWidth(450); // définition de la largeur préférée du Separator
        Text textInfos2Sneaker2 = new Text(
                "La Nike Dunk Low Off-White Futura UNC présente une base en cuir blanc, en accord avec le logo 'FL' sur le talon et le zip tie. Cette base s'accompagne de superpositions en cuir bleu clair, pour reprendre l'iconique coloris UNC toujours très apprécié. Un deuxième système de laçage orange vif et les célèbres inscriptions sur la face intérieures, caractéristiques d'Off-White, apportent la touche finale.");
        textInfos2Sneaker2.setFont(this.getPolicePersonnalisee(20));
        textInfos2Sneaker2.setWrappingWidth(650);
        textInfos2Sneaker2.setFill(Color.WHITE);
        textInfos2Sneaker2.setTextAlignment(TextAlignment.CENTER);
        Button boutonEnSavoirPlusSneaker2 = this.creeBoutonEnSavoirPlus("15");
        vboxInfosSneaker2.getChildren().addAll(textNomSneaker2, textInfos1Sneaker2, separator3, textInfos2Sneaker2,
                boutonEnSavoirPlusSneaker2);
        hboxSneaker2.getChildren().addAll(imageSneaker2, vboxInfosSneaker2);

        // margins
        VBox.setMargin(textNotreSelection, new Insets(50, 0, 0, 0));
        VBox.setMargin(separator, new Insets(0, 0, 150, 0));
        VBox.setMargin(hboxSneaker1, new Insets(0, 100, 100, 100));
        VBox.setMargin(textNomSneaker1, new Insets(0, 0, 60, 0));
        VBox.setMargin(hboxSneaker2, new Insets(0, 100, 100, 100));
        VBox.setMargin(textNomSneaker2, new Insets(0, 0, 60, 0));

        vboxNotreSelection.getChildren().addAll(textNotreSelection, separator, hboxSneaker1, hboxSneaker2);
        return vboxNotreSelection;
    }

    /**
     * Créé la partie "Un peu d'extravagence" (page Accueil)
     * 
     * @return HBox : la hbox de la partie "Un peu d'extravagence" (page Accueil)
     */
    public HBox creePartieUnPeuExtravagence() {
        HBox hboxUnPeuExtravagence = new HBox(90);
        hboxUnPeuExtravagence.setAlignment(Pos.CENTER);

        VBox vboxInfosSneaker = new VBox(20);
        vboxInfosSneaker.setAlignment(Pos.TOP_LEFT);
        Text textUnpeuExtravagence = new Text("UN PEU D'EXTRAVAGENCE !");
        textUnpeuExtravagence.setFont(this.getPolicePersonnalisee(45));
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: black");
        separator.setPrefHeight(3); // epaisseur du trait en pixels
        separator.setMaxWidth(650); // définition de la largeur préférée du Separator
        Text textNomSneaker = new Text("Big Red Boots");
        textNomSneaker.setFont(this.getPolicePersonnalisee(40));
        Text textInfos1Sneaker = new Text(
                "Les créations improbables de MSCHF ne sont plus à prouver. Toutefois, le collectif poursuit sa quête incessante d’exclusivité et dévoile cette fois-ci une conception simple mais indéniablement accrocheuse");
        textInfos1Sneaker.setFont(this.getPolicePersonnalisee(20));
        textInfos1Sneaker.setWrappingWidth(650);
        Button boutonEnSavoirPlusSneaker = this.creeBoutonEnSavoirPlus("16");
        vboxInfosSneaker.getChildren().addAll(textUnpeuExtravagence, separator, textNomSneaker, textInfos1Sneaker,
                boutonEnSavoirPlusSneaker);

        VBox vboxInfos2Sneaker = new VBox(30);
        vboxInfos2Sneaker.setAlignment(Pos.TOP_RIGHT);
        ImageView imageSneaker = this.creeImageSneaker("bigredbootmschf.png");
        Separator separator2 = new Separator(); // un séparateur pour l'esthétique uniquement
        separator2.setStyle("-fx-background-color: black");
        separator2.setPrefHeight(3); // epaisseur du trait en pixels
        separator2.setMaxWidth(500); // définition de la largeur préférée du Separator
        Text textInfos2Sneaker = new Text(
                "“Les Big Red Boots sont des bottes de VR Chat”. Si Mischief a la volonté de mélanger le virtuel et le réel, la silhouette extravagante tient indéniablement son inspiration des chaussures portées par le protagoniste du manga classique Astro Boy. À cet égard, la tige de la grosse botte rouge de MSCHF est construite presque entièrement en coque TPU, avec de la mousse EVA utilisée pour la semelle extérieure nervurée. Par ailleurs, la conception inédite est dotée d’un branding minimaliste MSCHF");
        textInfos2Sneaker.setFont(this.getPolicePersonnalisee(20));
        textInfos2Sneaker.setWrappingWidth(650);
        textInfos2Sneaker.setTextAlignment(TextAlignment.RIGHT);
        vboxInfos2Sneaker.getChildren().addAll(imageSneaker, separator2, textInfos2Sneaker);
        VBox.setMargin(separator2, new Insets(0, 50, 0, 0));
        VBox.setMargin(textNomSneaker, new Insets(30, 0, 30, 0));

        hboxUnPeuExtravagence.getChildren().addAll(vboxInfosSneaker, vboxInfos2Sneaker);
        return hboxUnPeuExtravagence;
    }

    /**
     * Retourne le gridPane des sneakers à partir d'une liste de Sneaker
     * 
     * @param lesSneakers List<Sneaker> : la liste des sneakers à afficher
     * @return GridPane : le gridPane contenant les ventes passsées en paramètres
     */
    public ScrollPane creerSneakersGridPane(List<Sneaker> lesSneakers) {
        GridPane gridPaneVentesSneakers = new GridPane();
        gridPaneVentesSneakers.setBackground(this.getBackgroundAutresPages());
        ScrollPane scrollPaneVentesSneakers = new ScrollPane(gridPaneVentesSneakers);
        scrollPaneVentesSneakers.setStyle("-fx-background-color: transparent; -fx-background-insets: 0;");
        scrollPaneVentesSneakers.setFitToWidth(true); // enlève la bordure droite du ScrollPane de base
        scrollPaneVentesSneakers.setFitToHeight(true); // ajuste la hauteur du contenu pour remplir l'espace disponible
        gridPaneVentesSneakers.setAlignment(Pos.CENTER);
        if (lesSneakers != null) {
            for (int i = 0; i < lesSneakers.size(); i++) {
                Sneaker sneakerActuelle = lesSneakers.get(i);
                AnchorPane anchorPane = new AnchorPane();
                Button boutonSneaker = new Button();
                boutonSneaker.setOnAction(new ControleurSneaker(this, sneakerActuelle));
                boutonSneaker.setPrefHeight(300);
                VBox vboxSneaker = new VBox(5);
                vboxSneaker.setAlignment(Pos.TOP_LEFT);
                ImageView imageLikes = null;
                try {
                    if (this.userSQL.sneakerBienEnFavori(this.utilisateurConnecte.getIdUser(),
                            sneakerActuelle.getIdSneaker())) {
                        imageLikes = new ImageView(new Image("file:./img/like_red.png"));
                    } else {
                        imageLikes = new ImageView(new Image("file:./img/like_black.png"));
                    }
                } catch (UtilisateurInvalideException e) {
                    System.out.println(e);
                } catch (SQLException e) {
                    System.out.println(e);
                }
                imageLikes.setFitWidth(25);
                imageLikes.setFitHeight(20);
                Text nomSneaker = new Text(sneakerActuelle.getNomSneaker());
                nomSneaker.setFont(this.getPolicePersonnalisee(20));
                nomSneaker.setWrappingWidth(300);
                sneakerActuelle.resetLesImages(); // reset la liste d'images de la sneaker actuelle
                try {
                    this.sneakerSQL.getPhoto(sneakerActuelle.getIdSneaker());
                    sneakerActuelle.ajouteImage("/img/img_" + sneakerActuelle.getIdSneaker() + "0.png");
                } catch (PhotoManquanteException e) { // la sneaker n'a aucune photo
                    sneakerActuelle.ajouteImage("/img/default.png");
                } catch (SQLException | IOException | SneakerInvalideException e) {
                    System.out.println(e);
                }
                ImageView imagePhoto = new ImageView(new Image("file:." + sneakerActuelle.getImageSneaker(0)));
                imagePhoto.setFitWidth(348);
                imagePhoto.setFitHeight(230);
                vboxSneaker.getChildren().addAll(imagePhoto, nomSneaker);
                AnchorPane.setTopAnchor(imageLikes, 8.0);
                AnchorPane.setLeftAnchor(imageLikes, 8.0);
                anchorPane.getChildren().addAll(vboxSneaker, imageLikes);
                boutonSneaker.setGraphic(anchorPane);
                gridPaneVentesSneakers.add(boutonSneaker, i % 5, i / 5);
                VBox.setMargin(imageLikes, new Insets(10, 0, 0, 5));
                VBox.setMargin(nomSneaker, new Insets(0, 0, 8, 5));
                GridPane.setMargin(boutonSneaker, new Insets(15, 8, 15, 8));
            }
        }
        return scrollPaneVentesSneakers;
    }

    /**
     * Retourne la VBox des sneakers dans le panier à partir d'une liste de Sneaker
     * 
     * @param lesSneakers List<Sneaker> : la liste des sneakers à afficher
     * @return VBox : la VBox contenant les sneakers passsées en paramètres
     */
    public VBox creerSneakersVBoxPanier(List<Sneaker> lesSneakers, Set<List<Integer>> lesTailles) {
        List<Integer> listeTailles = lesTailles.stream().flatMap(List::stream).collect(Collectors.toList());
        VBox vboxSneakersPanier = new VBox(35);
        vboxSneakersPanier.setAlignment(Pos.CENTER);
        if (lesSneakers != null) {
            for (int i = 0; i < lesSneakers.size(); i++) {
                final int index = i;
                Sneaker sneakerActuelle = lesSneakers.get(i);
                AnchorPane anchorPane = new AnchorPane();
                Button boutonSneaker = new Button();
                boutonSneaker.setOnAction(new ControleurSneaker(this, sneakerActuelle));
                HBox hboxSneaker = new HBox(10);
                hboxSneaker.setAlignment(Pos.TOP_LEFT);
                ImageView imageLikes = null;
                try {
                    if (this.userSQL.sneakerBienEnFavori(this.utilisateurConnecte.getIdUser(), sneakerActuelle.getIdSneaker())) {
                        imageLikes = new ImageView(new Image("file:./img/like_red.png"));
                    }
                    else {
                        imageLikes = new ImageView(new Image("file:./img/like_black.png"));
                    }
                }
                catch (UtilisateurInvalideException e) {
                    System.out.println(e);
                }
                catch (SQLException e) {
                    System.out.println(e);
                }
                imageLikes.setFitWidth(25);
                imageLikes.setFitHeight(20);
                VBox vboxInfosSneaker = new VBox(10);
                vboxInfosSneaker.setAlignment(Pos.TOP_LEFT);
                Text nomSneaker = new Text(sneakerActuelle.getNomSneaker());
                nomSneaker.setFont(this.getPolicePersonnalisee(20));
                nomSneaker.setWrappingWidth(700);
                Text textTaille = new Text("Taille : " + listeTailles.get(i));
                textTaille.setFont(this.getPolicePersonnalisee(20));
                textTaille.setWrappingWidth(700);
                int quantiteSneakerTaille = 1;
                try {
                    quantiteSneakerTaille = this.sneakerSQL.getQuantiteSneakerTaille(this.utilisateurConnecte.getIdUser(), listeTailles.get(i), sneakerActuelle.getIdSneaker());
                } catch (SQLException e) {
                    System.out.println(e);
                }
                Text textPrix = new Text((sneakerActuelle.getPrix() * quantiteSneakerTaille) + " euros");
                textPrix.setFont(this.getPolicePersonnalisee(25));
                sneakerActuelle.resetLesImages(); // reset la liste d'images de la sneaker actuelle
                try {
                    this.sneakerSQL.getPhoto(sneakerActuelle.getIdSneaker());
                    sneakerActuelle.ajouteImage("/img/img_" + sneakerActuelle.getIdSneaker() + "0.png");
                } catch (PhotoManquanteException e) { // la sneaker n'a aucune photo
                    sneakerActuelle.ajouteImage("/img/default.png");
                } catch (SQLException | IOException | SneakerInvalideException e) {
                    System.out.println(e);
                }
                ImageView imagePhoto = new ImageView(new Image("file:." + sneakerActuelle.getImageSneaker(0)));
                imagePhoto.setFitWidth(348);
                imagePhoto.setFitHeight(230);
                AnchorPane.setTopAnchor(imageLikes, 8.0);
                AnchorPane.setLeftAnchor(imageLikes, 8.0);
                anchorPane.getChildren().addAll(imagePhoto, imageLikes);
                boutonSneaker.setGraphic(anchorPane);
                vboxInfosSneaker.getChildren().addAll(nomSneaker, textTaille, textPrix);
                VBox vboxPartieDroite = new VBox();
                vboxPartieDroite.setAlignment(Pos.CENTER);
                Button boutonSupprimer = new Button();
                ImageView imageSupprimer = new ImageView(new Image("file:./img/cross.png"));
                imageSupprimer.setFitWidth(35);
                imageSupprimer.setFitHeight(30);
                boutonSupprimer.setGraphic(imageSupprimer);
                boutonSupprimer.setOnAction(new ControleurSupprimerSneakerPanier(this, sneakerActuelle, listeTailles.get(i)));
                HBox hboxQuantite = new HBox(5);
                hboxQuantite.setAlignment(Pos.CENTER);
                Text textQuantite = new Text("Qté");
                textQuantite.setFont(this.getPolicePersonnalisee(15));
                ComboBox<Integer> comboBox = new ComboBox<>(FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
                comboBox.getSelectionModel().select(quantiteSneakerTaille - 1);
                comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                    // exécute la méthode pour modifier la base de données
                    try {
                        this.sneakerSQL.modifieQuantiteSneakerTaille(this.utilisateurConnecte.getIdUser(),listeTailles.get(index), sneakerActuelle.getIdSneaker(), newValue);
                        textPrix.setText((sneakerActuelle.getPrix() * this.sneakerSQL.getQuantiteSneakerTaille(this.utilisateurConnecte.getIdUser(), listeTailles.get(index), sneakerActuelle.getIdSneaker())) + " euros");
                    } catch (SQLException e) {
                        System.out.println(e);
                    }
                });
                this.creeHoverBouton(boutonSupprimer, "#FFFFFF", "#C4C3C3");
                hboxQuantite.getChildren().addAll(textQuantite, comboBox);
                vboxPartieDroite.getChildren().addAll(boutonSupprimer, hboxQuantite);
                hboxSneaker.getChildren().addAll(boutonSneaker, vboxInfosSneaker, vboxPartieDroite);
                vboxSneakersPanier.getChildren().add(hboxSneaker); // ajout de la sneaker actuelle
                VBox.setMargin(textPrix, new Insets(142, 0, 0, 0));
                VBox.setMargin(hboxQuantite, new Insets(180, 0, 0, 0));
                VBox.setMargin(hboxSneaker, new Insets(0, 350, 0, 350));
            }
        }
        return vboxSneakersPanier;
    }

    /**
     * change la fenetre pour la page Connexion
     */
    public void modeConnexion() {
        // réinitialisation des éléments
        this.panelCentral = new BorderPane();
        this.panelCentral.setBackground(this.getBackgroundConnexionInscription());
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setMaxWidth(1200);
        gridPane.setMaxHeight(900);
        gridPane.setStyle("-fx-background-color : #8787DE; -fx-background-radius: 15");

        // création du Text Connexion
        StackPane textPane = new StackPane();
        Text textConnexion = new Text("Connexion");
        textConnexion.setFill(Color.WHITE);
        textConnexion.setFont(this.getPolicePersonnalisee(80));
        textPane.getChildren().add(textConnexion);
        StackPane.setAlignment(textConnexion, Pos.CENTER);
        gridPane.add(textPane, 0, 0);

        // création du séparateur en-dessous du text Connexion
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: white");
        separator.setPrefHeight(5); // epaisseur du trait en pixels
        separator.setPrefWidth(600); // définition de la largeur préférée du Separator
        gridPane.add(separator, 0, 1);

        // création de la partie "Nom d'utilisateur"
        Text textNom = new Text("NOM D'UTILISATEUR");
        textNom.setFill(Color.WHITE);
        textNom.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textNom, 0, 2);
        TextField tfNomUtilisateur = new TextField("");
        tfNomUtilisateur.setPrefHeight(30);
        tfNomUtilisateur.setStyle("-fx-background-radius: 8");
        tfNomUtilisateur.setPromptText("Entrez votre nom d'utilisateur...");
        this.lesElementsGraphiques.put("tfNomUtilisateur", tfNomUtilisateur);
        gridPane.add(tfNomUtilisateur, 0, 3);

        // création de la partie "Mot de passe"
        Text textMdp = new Text("MOT DE PASSE");
        textMdp.setFill(Color.WHITE);
        textMdp.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textMdp, 0, 4);
        PasswordField passwdf = new PasswordField();
        passwdf.setPrefHeight(30);
        passwdf.setStyle("-fx-background-radius: 8");
        passwdf.setPromptText("Entrez votre mot de passe...");
        this.lesElementsGraphiques.put("passwdf", passwdf);
        gridPane.add(passwdf, 0, 5);
        Button boutonMdp = new Button();
        boutonMdp.setStyle("-fx-background-color: #F1C22B; -fx-background-radius: 10");
        boutonMdp
                .setOnMouseEntered(e -> boutonMdp.setStyle("-fx-background-color: #B79623; -fx-background-radius: 10")); // style
                                                                                                                         // lors
                                                                                                                         // du
                                                                                                                         // survol
        boutonMdp.setOnMouseExited(e -> boutonMdp.setStyle("-fx-background-color: #F1C22B; -fx-background-radius: 10")); // réinitialisation
                                                                                                                         // du
                                                                                                                         // style
        boutonMdp.setId("non-visible");
        boutonMdp.setOnAction(new ControleurMdpVisible(this));
        ImageView imageMdpNonVisible = new ImageView(new Image("file:./img/hide.png"));
        imageMdpNonVisible.setFitHeight(20);
        imageMdpNonVisible.setFitWidth(20);
        boutonMdp.setGraphic(imageMdpNonVisible);
        this.lesElementsGraphiques.put("imageMdpNonVisible", imageMdpNonVisible);
        this.lesElementsGraphiques.put("boutonMdp", boutonMdp);
        ImageView imageMdpVisible = new ImageView(new Image("file:./img/view.png"));
        imageMdpVisible.setFitHeight(20);
        imageMdpVisible.setFitWidth(20);
        this.lesElementsGraphiques.put("imageMdpVisible", imageMdpVisible);
        gridPane.add(boutonMdp, 1, 5);

        // création du Text "Mot de passe oublié ?"
        Text textCliquable = new Text("Mot de passe oublié ?");
        textCliquable.setUnderline(true);
        textCliquable.setFill(Color.WHITE);
        textCliquable.setFont(this.getPolicePersonnalisee(20));
        textCliquable.setOnMouseEntered(e -> textCliquable.setFill(Color.BLACK)); // style lors du survol
        textCliquable.setOnMouseExited(e -> textCliquable.setFill(Color.WHITE)); // réinitialisation du style
        textCliquable.setOnMouseClicked(new ControleurMotDePasseOublie(this));
        gridPane.add(textCliquable, 0, 6);

        // création du bouton "Connexion"
        Button boutonConnexion = new Button("Connexion");
        // définition de l'alignement horizontal et vertical du bouton au centre du
        // GridPane
        GridPane.setHalignment(boutonConnexion, HPos.CENTER);
        GridPane.setValignment(boutonConnexion, VPos.CENTER);
        boutonConnexion.setOnAction(new ControleurConnexion(this));
        boutonConnexion.setPrefWidth(500);
        this.creeHoverBouton(boutonConnexion, "#F1C22B", "#B79623");
        boutonConnexion.setTextFill(Color.WHITE);
        boutonConnexion.setFont(this.getPolicePersonnalisee(30));
        gridPane.add(boutonConnexion, 0, 7);

        // création du bouton "Je n'ai pas de compte"
        Button boutonInscription = new Button("Je n'ai pas de compte");
        GridPane.setHalignment(boutonInscription, HPos.CENTER);
        GridPane.setValignment(boutonInscription, VPos.CENTER);
        boutonInscription.setOnAction(new ControleurInscription(this));
        boutonInscription.setPrefWidth(500);
        this.creeHoverBouton(boutonInscription, "#6A6AB2", "#3C3C61");
        boutonInscription.setTextFill(Color.WHITE);
        boutonInscription.setFont(this.getPolicePersonnalisee(30));
        gridPane.add(boutonInscription, 0, 8);

        // création des marges
        GridPane.setMargin(textNom, new Insets(80, 0, 5, 0));
        GridPane.setMargin(textMdp, new Insets(80, 0, 5, 0));
        GridPane.setMargin(textCliquable, new Insets(10, 0, 0, 0));
        GridPane.setMargin(boutonConnexion, new Insets(100, 0, 50, 0));

        this.panelCentral.setCenter(gridPane);
        this.fenetre.setTop(this.titreConnexionInscription());
        this.fenetre.setCenter(this.panelCentral);
    }

    /**
     * change la fenetre pour la page Inscription
     */
    public void modeInscription() {
        // réinitialisation de la fenetre
        this.panelCentral = new BorderPane();
        this.panelCentral.setBackground(this.getBackgroundConnexionInscription());
        GridPane gridPane = new GridPane();

        // création et gestion des paramètres de la GridPane
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setMaxWidth(1200);
        gridPane.setMaxHeight(900);
        gridPane.setStyle("-fx-background-color : #8787DE; -fx-background-radius: 15");

        // création du Text Inscription
        StackPane textPane = new StackPane();
        Text textInscription = new Text("Inscription");
        textInscription.setFill(Color.WHITE);
        textInscription.setFont(this.getPolicePersonnalisee(80));
        textPane.getChildren().add(textInscription);
        StackPane.setAlignment(textInscription, Pos.CENTER);
        gridPane.add(textPane, 0, 0);

        // création du séparateur en-dessous du text Connexion
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: white");
        separator.setPrefHeight(5); // epaisseur du trait en pixels
        separator.setPrefWidth(600); // définition de la largeur préférée du Separator
        gridPane.add(separator, 0, 1);

        // création de la partie "Nom d'utilisateur"
        Text textNom = new Text("NOM D'UTILISATEUR");
        textNom.setFill(Color.WHITE);
        textNom.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textNom, 0, 2);
        TextField tfNomUtilisateur = new TextField("");
        this.lesElementsGraphiques.put("tfNomUtilisateur", tfNomUtilisateur);
        tfNomUtilisateur.setStyle("-fx-background-radius: 8");
        tfNomUtilisateur.setPromptText("Entrez un nom d'utilisateur...");
        tfNomUtilisateur.setMaxWidth(600);
        gridPane.add(tfNomUtilisateur, 0, 3);

        // création de la partie "Adresse e-mail"
        Text textMail = new Text("ADRESSE E-MAIL");
        textMail.setFill(Color.WHITE);
        textMail.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textMail, 0, 4);
        TextField tfMail = new TextField("");
        this.lesElementsGraphiques.put("tfMail", tfMail);
        tfMail.setStyle("-fx-background-radius: 8");
        tfMail.setPromptText("Entrez un e-mail...");
        tfMail.setMaxWidth(600);
        gridPane.add(tfMail, 0, 5);

        // création de la partie "Mot de passe"
        Text textMdp = new Text("MOT DE PASSE");
        textMdp.setFill(Color.WHITE);
        textMdp.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textMdp, 0, 6);
        PasswordField passwdf = new PasswordField();
        this.lesElementsGraphiques.put("passwdf", passwdf);
        passwdf.setStyle("-fx-background-radius: 8");
        passwdf.setPromptText("Entrez un mot de passe...");
        passwdf.setMaxWidth(600);
        gridPane.add(passwdf, 0, 7);

        // création de la partie "Confirmation"
        Text textConfirmation = new Text("CONFIRMATION DU MOT DE PASSE");
        textConfirmation.setFill(Color.WHITE);
        textConfirmation.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textConfirmation, 0, 8);
        PasswordField passwdConfirmation = new PasswordField();
        this.lesElementsGraphiques.put("passwdConfirmation", passwdConfirmation);
        passwdConfirmation.setStyle("-fx-background-radius: 8");
        passwdConfirmation.setPromptText("Confirmez votre mot de passe...");
        passwdConfirmation.setMaxWidth(600);
        gridPane.add(passwdConfirmation, 0, 9);

        // création du bouton "Inscription"
        Button boutonValider = new Button("Inscription");
        GridPane.setHalignment(boutonValider, HPos.CENTER);
        GridPane.setValignment(boutonValider, VPos.CENTER);
        boutonValider.setOnAction(new ControleurInscription(this));
        boutonValider.setPrefWidth(500);
        this.creeHoverBouton(boutonValider, "#F1C22B", "#B79623");
        boutonValider.setTextFill(Color.WHITE);
        boutonValider.setFont(this.getPolicePersonnalisee(30));
        gridPane.add(boutonValider, 0, 10);

        // création du bouton "J'ai déjà un compte"
        Button boutonConnexion = new Button("J'ai déjà un compte");
        GridPane.setHalignment(boutonConnexion, HPos.CENTER);
        GridPane.setValignment(boutonConnexion, VPos.CENTER);
        boutonConnexion.setOnAction(new ControleurConnexion(this));
        boutonConnexion.setPrefWidth(500);
        this.creeHoverBouton(boutonConnexion, "#6A6AB2", "#3C3C61");
        boutonConnexion.setTextFill(Color.WHITE);
        boutonConnexion.setFont(this.getPolicePersonnalisee(30));
        gridPane.add(boutonConnexion, 0, 11);

        // création des marges
        GridPane.setMargin(textNom, new Insets(40, 0, 5, 0));
        GridPane.setMargin(textMail, new Insets(40, 0, 5, 0));
        GridPane.setMargin(textMdp, new Insets(40, 0, 5, 0));
        GridPane.setMargin(textConfirmation, new Insets(40, 0, 5, 0));
        GridPane.setMargin(boutonValider, new Insets(100, 0, 50, 0));

        this.panelCentral.setCenter(gridPane);
        this.fenetre.setTop(this.titreConnexionInscription());
        this.fenetre.setCenter(this.panelCentral);
    }

    /**
     * change la fenetre pour la page Mot de passe oublié
     */
    public void modeMotDePasseOublie() {
        // réinitialisation de la fenetre
        this.panelCentral = new BorderPane();
        this.panelCentral.setBackground(this.getBackgroundConnexionInscription());
        GridPane gridPane = new GridPane();

        // création et gestion des paramètres de la GridPane
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setMaxWidth(1200);
        gridPane.setMaxHeight(900);
        gridPane.setStyle("-fx-background-color : #8787DE; -fx-background-radius: 15");

        // création du Text Inscription
        StackPane textPane = new StackPane();
        Text textInscription = new Text("Mot de passe oublié");
        textInscription.setFill(Color.WHITE);
        textInscription.setFont(this.getPolicePersonnalisee(80));
        textPane.getChildren().add(textInscription);
        StackPane.setAlignment(textInscription, Pos.CENTER);
        gridPane.add(textPane, 0, 0);

        // création du séparateur en-dessous du text Connexion
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: white");
        separator.setPrefHeight(5); // epaisseur du trait en pixels
        separator.setPrefWidth(600); // définition de la largeur préférée du Separator
        gridPane.add(separator, 0, 1);

        // création Text informatif
        StackPane textPane2 = new StackPane();
        Text textInformatif = new Text("PROCÉDURE DE RÉINITIALISATION DU MOT DE PASSE");
        textInformatif.setFill(Color.RED);
        textInformatif.setFont(this.getPolicePersonnalisee(25));
        textPane2.getChildren().add(textInformatif);
        StackPane.setAlignment(textInformatif, Pos.CENTER);
        gridPane.add(textPane2, 0, 2);

        // création de la partie "Nom d'utilisateur"
        Text textNom = new Text("NOM D'UTILISATEUR");
        textNom.setFill(Color.WHITE);
        textNom.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textNom, 0, 3);
        TextField tfNomUtilisateur = new TextField("");
        this.lesElementsGraphiques.put("tfNomUtilisateur", tfNomUtilisateur);
        tfNomUtilisateur.setStyle("-fx-background-radius: 8");
        tfNomUtilisateur.setPromptText("Entrez votre nom d'utilisateur...");
        tfNomUtilisateur.setMaxWidth(600);
        gridPane.add(tfNomUtilisateur, 0, 4);

        // création de la partie "Adresse e-mail"
        Text textMail = new Text("ADRESSE E-MAIL");
        textMail.setFill(Color.WHITE);
        textMail.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textMail, 0, 5);
        TextField tfMail = new TextField("");
        this.lesElementsGraphiques.put("tfMail", tfMail);
        tfMail.setStyle("-fx-background-radius: 8");
        tfMail.setPromptText("Entrez votre e-mail...");
        tfMail.setMaxWidth(600);
        gridPane.add(tfMail, 0, 6);

        // création de la partie "Mot de passe"
        Text textMdp = new Text("NOUVEAU MOT DE PASSE");
        textMdp.setFill(Color.WHITE);
        textMdp.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textMdp, 0, 7);
        PasswordField passwdf = new PasswordField();
        this.lesElementsGraphiques.put("passwdf", passwdf);
        passwdf.setStyle("-fx-background-radius: 8");
        passwdf.setPromptText("Entrez votre nouveau mot de passe...");
        passwdf.setMaxWidth(600);
        gridPane.add(passwdf, 0, 8);

        // création de la partie "Confirmation"
        Text textConfirmation = new Text("CONFIRMATION DU MOT DE PASSE");
        textConfirmation.setFill(Color.WHITE);
        textConfirmation.setFont(this.getPolicePersonnalisee(35));
        gridPane.add(textConfirmation, 0, 9);
        PasswordField passwdConfirmation = new PasswordField();
        this.lesElementsGraphiques.put("passwdConfirmation", passwdConfirmation);
        passwdConfirmation.setStyle("-fx-background-radius: 8");
        passwdConfirmation.setPromptText("Confirmez votre nouveau mot de passe...");
        passwdConfirmation.setMaxWidth(600);
        gridPane.add(passwdConfirmation, 0, 10);

        // création du bouton "Inscription"
        Button boutonValider = new Button("Valider");
        GridPane.setHalignment(boutonValider, HPos.CENTER);
        GridPane.setValignment(boutonValider, VPos.CENTER);
        boutonValider.setOnAction(new ControleurConnexion(this));
        boutonValider.setPrefWidth(500);
        this.creeHoverBouton(boutonValider, "#F70F0F", "#9E1919");
        boutonValider.setTextFill(Color.WHITE);
        boutonValider.setFont(this.getPolicePersonnalisee(30));
        gridPane.add(boutonValider, 0, 11);

        // création du bouton "J'ai déjà un compte"
        Button boutonConnexion = new Button("Annuler la procédure");
        GridPane.setHalignment(boutonConnexion, HPos.CENTER);
        GridPane.setValignment(boutonConnexion, VPos.CENTER);
        boutonConnexion.setOnAction(new ControleurConnexion(this));
        boutonConnexion.setPrefWidth(500);
        this.creeHoverBouton(boutonConnexion, "#6A6AB2", "#3C3C61");
        boutonConnexion.setTextFill(Color.WHITE);
        boutonConnexion.setFont(this.getPolicePersonnalisee(30));
        gridPane.add(boutonConnexion, 0, 12);

        // création des marges
        GridPane.setMargin(textPane2, new Insets(10, 0, 0, 0));
        GridPane.setMargin(textNom, new Insets(40, 0, 5, 0));
        GridPane.setMargin(textMail, new Insets(40, 0, 5, 0));
        GridPane.setMargin(textMdp, new Insets(40, 0, 5, 0));
        GridPane.setMargin(textConfirmation, new Insets(40, 0, 5, 0));
        GridPane.setMargin(boutonValider, new Insets(100, 0, 50, 0));

        this.panelCentral.setCenter(gridPane);
        this.fenetre.setTop(this.titreConnexionInscription());
        this.fenetre.setCenter(this.panelCentral);
    }

    /**
     * change la fenetre pour la page Favori
     */
    public void modeFavori() {
        // réinitialisation de la fenetre
        this.panelCentral = new BorderPane();
        this.fenetre.setBackground(this.getBackgroundAutresPages());

        // Text (top du panelCentral)
        List<Sneaker> lesSneakersFavorites = this.sneakerSQL.sneakersFavori(this.utilisateurConnecte.getPseudo());
        int nombreSneakersFavoris = lesSneakersFavorites.size(); // récupère le nombre de sneakers en favori de
                                                                 // l'utilisateur connecté
        Text textAccueil = new Text("Votre liste de favoris est tristement vide");
        if (nombreSneakersFavoris > 0) { // si l'utilisateur connecté à au moins 1 sneaker en favori
            textAccueil.setText("Faites de vos coups de coeur une réalité");
        }
        textAccueil.setFont(this.getPolicePersonnalisee(60));
        textAccueil.setFill(Color.BLACK);
        textAccueil.setUnderline(true);
        BorderPane.setAlignment(textAccueil, Pos.CENTER);
        BorderPane.setMargin(textAccueil, new Insets(50, 0, 50, 0));
        this.panelCentral.setTop(textAccueil);

        // GridPane (center de la panelCentral)
        this.panelCentral.setCenter(this.creerSneakersGridPane(lesSneakersFavorites));

        // ajout du bandeau inférieur (en bas) sur la page
        // this.panelCentral.setBottom(this.bandeauInferieurAutresPages());

        this.fenetre.setTop(this.titreAutresPages());
        this.fenetre.setCenter(this.panelCentral);
    }

    /**
     * Modifie l'affichage de la page Profil pour pouvoir éditer les informations
     */
    public void majmodeProfilEditable() {
        TextField reponseNomUtilisateur = (TextField) this.lesElementsGraphiques.get("reponseNomUtilisateur");
        TextField reponseMail = (TextField) this.lesElementsGraphiques.get("reponseMail");
        TextField reponseMdp = (TextField) this.lesElementsGraphiques.get("reponseMdp");
        reponseNomUtilisateur.setEditable(true);
        reponseMail.setEditable(true);
        reponseMdp.setEditable(true);
        Button boutonChangerInfos = (Button) this.lesElementsGraphiques.get("boutonChangerInfos");
        boutonChangerInfos.setDisable(true);
        Button boutonCross = (Button) this.lesElementsGraphiques.get("boutonCross");
        boutonCross.setDisable(false);
        Button boutonCheck = (Button) this.lesElementsGraphiques.get("boutonCheck");
        boutonCheck.setDisable(false);
        this.fenetre.setCenter(this.panelCentral);
    }

    /**
     * Modifie l'affichage de la page Profil pour pouvoir ne plus éditer les
     * informations
     */
    public void majmodeProfilNonEditable() {
        TextField reponseNomUtilisateur = (TextField) this.lesElementsGraphiques.get("reponseNomUtilisateur");
        TextField reponseMail = (TextField) this.lesElementsGraphiques.get("reponseMail");
        TextField reponseMdp = (TextField) this.lesElementsGraphiques.get("reponseMdp");
        reponseNomUtilisateur.setText(this.utilisateurConnecte.getPseudo());
        reponseMail.setText(this.utilisateurConnecte.getEmail());
        reponseMdp.setText(this.utilisateurConnecte.getPassword());
        reponseNomUtilisateur.setEditable(false);
        reponseMail.setEditable(false);
        reponseMdp.setEditable(false);
        Button boutonChangerInfos = (Button) this.lesElementsGraphiques.get("boutonChangerInfos");
        boutonChangerInfos.setText("Changer mes informations");
        boutonChangerInfos.setDisable(false);
        Button boutonCross = (Button) this.lesElementsGraphiques.get("boutonCross");
        boutonCross.setDisable(true);
        Button boutonCheck = (Button) this.lesElementsGraphiques.get("boutonCheck");
        boutonCheck.setDisable(true);
        this.fenetre.setCenter(this.panelCentral);
    }

    /**
     * change la fenetre pour la page Profil
     */
    public void modeProfil() {
        // réinitialisation de la fenetre
        this.panelCentral = new BorderPane();

        // BorderPane (left du panelCentral)
        BorderPane borderPanePartieGauche = new BorderPane();
        BorderPane.setMargin(borderPanePartieGauche, new Insets(50, 0, 50, 50));
        borderPanePartieGauche.setStyle("-fx-background-color: #8787DE; -fx-background-radius: 15");
        VBox vboxTopProfil = new VBox(10);
        vboxTopProfil.setAlignment(Pos.CENTER);
        Text textProfil = new Text("Votre profil");
        textProfil.setFont(this.getPolicePersonnalisee(35));
        Separator separatorProfil = new Separator(); // un séparateur pour l'esthétique uniquement
        separatorProfil.setStyle("-fx-background-color: black");
        separatorProfil.setPrefHeight(3); // epaisseur du trait en pixels
        vboxTopProfil.getChildren().addAll(textProfil, separatorProfil);
        borderPanePartieGauche.setTop(vboxTopProfil);
        BorderPane.setMargin(vboxTopProfil, new Insets(30, 50, 20, 30));
        VBox vboxLeft = new VBox(20);
        vboxLeft.setAlignment(Pos.TOP_CENTER);
        try {
            this.userSQL.getPhoto(this.utilisateurConnecte.getIdUser(), this.utilisateurConnecte.getPseudo());
            System.out.println("oui");
            this.utilisateurConnecte.setPhotoProfil(
                    "/img/" + this.utilisateurConnecte.getIdUser() + this.utilisateurConnecte.getPseudo() + ".png");
        } catch (PhotoManquanteException e) { // l'utilisateur n'a pas de photo de profil
            this.utilisateurConnecte.setPhotoProfil("/img/null.png");
        } catch (SQLException | UtilisateurInvalideException | IOException e) {
            System.out.println(e);
        }
        ImageView imagePhoto = new ImageView(new Image("file:." + this.utilisateurConnecte.getphotoProfil()));
        imagePhoto.setFitWidth(250);
        imagePhoto.setFitHeight(250);
        Button boutonChangerPhoto = new Button("Changer ma photo");
        boutonChangerPhoto.setOnAction(new ControleurChangerPhotoProfil(this));
        boutonChangerPhoto.setPrefWidth(200);
        boutonChangerPhoto.setPrefHeight(30);
        this.creeHoverBouton(boutonChangerPhoto, "#F1C22B", "#B79623");
        boutonChangerPhoto.setFont(this.getPolicePersonnalisee(20));
        vboxLeft.getChildren().addAll(imagePhoto, boutonChangerPhoto);
        VBox.setMargin(imagePhoto, new Insets(80, 50, 0, 50));
        borderPanePartieGauche.setLeft(vboxLeft);
        this.panelCentral.setLeft(borderPanePartieGauche);

        // VBox (right du panelCentral)
        VBox vboxRight = new VBox(0);
        BorderPane.setMargin(vboxRight, new Insets(130, 80, 130, 10));
        vboxRight.setStyle("-fx-background-color: #8787DE; -fx-background-radius: 15");
        vboxRight.setAlignment(Pos.CENTER);
        Text textInfos = new Text("Informations personnelles");
        textInfos.setFont(this.getPolicePersonnalisee(35));
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: black");
        separator.setPrefHeight(3); // epaisseur du trait en pixels
        GridPane gridPane = new GridPane();
        gridPane.setHgap(15); // espacement horizontal de 20 pixels
        gridPane.setVgap(15); // espacement vertical de 20 pixels
        Text textNomUtilisateur = new Text("NOM D'UTILISATEUR :");
        textNomUtilisateur.setFont(this.getPolicePersonnalisee(20));
        TextField reponseNomUtilisateur = new TextField(this.utilisateurConnecte.getPseudo());
        this.lesElementsGraphiques.put("reponseNomUtilisateur", reponseNomUtilisateur);
        reponseNomUtilisateur.setEditable(false);
        reponseNomUtilisateur.setFont(Font.font("Arial", 20));
        Text textMail = new Text("ADRESSE E-MAIL :");
        textMail.setFont(this.getPolicePersonnalisee(20));
        TextField reponseMail = new TextField(this.utilisateurConnecte.getEmail());
        this.lesElementsGraphiques.put("reponseMail", reponseMail);
        reponseMail.setEditable(false);
        reponseMail.setFont(Font.font("Arial", 20));
        Text textMdp = new Text("MOT DE PASSE :");
        textMdp.setFont(this.getPolicePersonnalisee(20));
        PasswordField reponseMdp = new PasswordField();
        this.lesElementsGraphiques.put("reponseMdp", reponseMdp);
        reponseMdp.setText(this.utilisateurConnecte.getPassword());
        reponseMdp.setEditable(false);
        reponseMdp.setFont(Font.font("Arial", 20));
        gridPane.add(textNomUtilisateur, 0, 0);
        gridPane.add(reponseNomUtilisateur, 0, 1);
        GridPane.setMargin(reponseNomUtilisateur, new Insets(0, 0, 50, 0));
        gridPane.add(textMail, 0, 2);
        gridPane.add(reponseMail, 0, 3);
        GridPane.setMargin(reponseMail, new Insets(0, 0, 50, 0));
        gridPane.add(textMdp, 0, 4);
        gridPane.add(reponseMdp, 0, 5);
        HBox hboxBoutons = new HBox(15);
        hboxBoutons.setAlignment(Pos.CENTER);
        Button boutonChangerInfos = new Button("Changer mes informations");
        this.lesElementsGraphiques.put("boutonChangerInfos", boutonChangerInfos);
        boutonChangerInfos.setOnAction(new ControleurChangerInfos(this));
        boutonChangerInfos.setPrefWidth(280);
        boutonChangerInfos.setPrefHeight(50);
        this.creeHoverBouton(boutonChangerInfos, "#F1C22B", "#B79623");
        boutonChangerInfos.setTextFill(Color.BLACK);
        boutonChangerInfos.setFont(this.getPolicePersonnalisee(20));
        Button boutonCross = new Button();
        boutonCross.setDisable(true);
        ImageView imageCross = new ImageView(new Image("file:./img/cross.png"));
        imageCross.setFitWidth(35);
        imageCross.setFitHeight(30);
        boutonCross.setGraphic(imageCross);
        this.lesElementsGraphiques.put("boutonCross", boutonCross);
        boutonCross.setOnAction(new ControleurAnnuler(this));
        this.creeHoverBouton(boutonCross, "#F1C22B", "#B79623");
        Button boutonCheck = new Button();
        boutonCheck.setDisable(true);
        ImageView imageCheck = new ImageView(new Image("file:./img/check.png"));
        imageCheck.setFitWidth(35);
        imageCheck.setFitHeight(30);
        boutonCheck.setGraphic(imageCheck);
        this.lesElementsGraphiques.put("boutonCheck", boutonCheck);
        boutonCheck.setOnAction(new ControleurValider(this));
        this.creeHoverBouton(boutonCheck, "#F1C22B", "#B79623");
        hboxBoutons.getChildren().addAll(boutonCross, boutonChangerInfos, boutonCheck);
        vboxRight.getChildren().addAll(textInfos, separator, gridPane, hboxBoutons);
        VBox.setMargin(textInfos, new Insets(30, 50, 5, 50));
        VBox.setMargin(separator, new Insets(0, 100, 50, 100));
        VBox.setMargin(gridPane, new Insets(0, 20, 100, 20));
        VBox.setMargin(hboxBoutons, new Insets(0, 0, 30, 0));
        this.panelCentral.setRight(vboxRight);

        this.fenetre.setTop(this.titreAutresPages());
        this.fenetre.setCenter(this.panelCentral);
    }

    /**
     * change la fenetre pour la page Panier
     */
    public void modePanier() {
        // réinitialisation de la fenetre
        this.panelCentral = new BorderPane();
        this.panelCentral.setStyle("-fx-background-color : white");
        ScrollPane scrollPanePanelCentral = new ScrollPane(this.panelCentral);
        scrollPanePanelCentral.setStyle("-fx-background-color: transparent; -fx-background-insets: 0;");
        scrollPanePanelCentral.setFitToWidth(true); // enlève la bordure droite du ScrollPane de base
        scrollPanePanelCentral.setFitToHeight(true); // enlève la bordure basse du ScrollPane de base

        // Text (top du panelCentral)
        Map<List<Integer>, List<Sneaker>> lesSneakersPanier = this.sneakerSQL
                .sneakersPanier(this.utilisateurConnecte.getPseudo());
        Collection<List<Sneaker>> collectionLesSneakers = lesSneakersPanier.values();
        List<Sneaker> lesSneakers = new ArrayList<>();
        for (List<Sneaker> liste : collectionLesSneakers) {
            lesSneakers = liste;
        }
        int nombreSneakersPanier = lesSneakers.size(); // récupère le nombre de sneakers dans le panier de l'utilisateur
                                                       // connecté
        Text textAccueil = new Text("Votre panier est tristement vide");
        if (nombreSneakersPanier > 0) { // si l'utilisateur connecté à au moins 1 sneaker dans le panier
            textAccueil.setText("Une carte bleue et c'est à vous !");
        }
        textAccueil.setFont(this.getPolicePersonnalisee(60));
        textAccueil.setFill(Color.BLACK);
        textAccueil.setUnderline(true);
        BorderPane.setAlignment(textAccueil, Pos.CENTER);
        BorderPane.setMargin(textAccueil, new Insets(50, 0, 50, 0));
        this.panelCentral.setTop(textAccueil);

        // GridPane (center de la panelCentral)
        VBox vboxSneakersPanier = this.creerSneakersVBoxPanier(lesSneakers, lesSneakersPanier.keySet());
        this.panelCentral.setCenter(vboxSneakersPanier);

        // ajout du bandeau inférieur (en bas) sur la page
        // this.panelCentral.setBottom(this.bandeauInferieurAutresPages());

        // margins
        BorderPane.setMargin(vboxSneakersPanier, new Insets(0, 0, 100, 0));

        this.fenetre.setTop(this.titreAutresPages());
        this.fenetre.setCenter(scrollPanePanelCentral);
    }

    /**
     * change la fenetre pour la page d'une Sneaker
     * 
     * @param sneaker : la sneaker choisie
     */
    public void modeSneaker(Sneaker sneaker) {
        // réinitialisation de la fenetre
        this.panelCentral = new BorderPane();
        this.panelCentral.setStyle("-fx-background-color : white");
        ScrollPane scrollPanePanelCentral = new ScrollPane(this.panelCentral);
        scrollPanePanelCentral.setStyle("-fx-background-color: transparent; -fx-background-insets: 0;");
        scrollPanePanelCentral.setFitToWidth(true); // enlève la bordure droite du ScrollPane de base
        scrollPanePanelCentral.setFitToHeight(true); // enlève la bordure basse du ScrollPane de base

        // partie gauche de la page d'une sneaker
        VBox vboxPartieGauchePageSneaker = this.creePartieGauchePageSneaker(sneaker);
        this.panelCentral.setLeft(vboxPartieGauchePageSneaker);
        // partie droite de la page d'une sneaker
        VBox vboxPartieDroitePageSneaker = this.creePartieDroitePageSneaker(sneaker);
        this.panelCentral.setRight(vboxPartieDroitePageSneaker);

        // margins
        BorderPane.setMargin(vboxPartieGauchePageSneaker, new Insets(50, 100, 100, 80));
        BorderPane.setMargin(vboxPartieDroitePageSneaker, new Insets(50, 80, 100, 0));

        this.fenetre.setTop(this.titreAutresPages());
        this.fenetre.setCenter(scrollPanePanelCentral);
    }

    /**
     * Créé partie gauche de la page Sneaker
     * 
     * @param sneaker : la sneaker choisie
     * @return VBox : la vbox contenant la partie gauche de la page Sneaker
     */
    public VBox creePartieGauchePageSneaker(Sneaker sneaker) {
        VBox vboxPartieGauchePageSneaker = new VBox(20);
        vboxPartieGauchePageSneaker.setAlignment(Pos.TOP_LEFT);
        AnchorPane anchorPane = new AnchorPane();
        Button boutonLikes = new Button();
        boutonLikes.setStyle("-fx-background-color: transparent; -fx-background-insets: 0;");
        this.lesElementsGraphiques.put("boutonLikes", boutonLikes);
        try {
            if (this.userSQL.sneakerBienEnFavori(this.utilisateurConnecte.getIdUser(), sneaker.getIdSneaker())) {
                boutonLikes.setId("rouge");
                this.changeImageFavori("like_red.png");
            } else {
                boutonLikes.setId("noir");
                this.changeImageFavori("like_black.png");
            }
        } catch (UtilisateurInvalideException e) {
        } catch (SQLException e) {
        }
        boutonLikes.setOnAction(new ControleurGererFavori(this, sneaker));
        int nbImagesSneaker = 0;
        sneaker.resetLesImages(); // reset la liste d'images de la sneaker
        try {
            nbImagesSneaker = this.sneakerSQL.getNbPhotos(sneaker.getIdSneaker());
            this.sneakerSQL.getPhoto(sneaker.getIdSneaker());
            for (int i = 0; i < 3; i++) {
                if (i < nbImagesSneaker) { // si l'indice actuel existe dans la liste d'images de la sneaker
                    sneaker.ajouteImage("/img/img_" + sneaker.getIdSneaker() + i + ".png");
                } else { // si il n'y a pas assez d'images pour la sneaker -> on met une image vide
                    sneaker.ajouteImage("/img/default.png");
                }
            }
        } catch (PhotoManquanteException e) { // la sneaker n'a aucune photo
            for (int i = 0; i < 3; i++) {
                sneaker.ajouteImage("/img/default.png");
            }
        } catch (SQLException | IOException | SneakerInvalideException e) {
            System.out.println(e);
        }
        ImageView image1Sneaker = new ImageView(new Image("file:." + sneaker.getImageSneaker(0)));
        image1Sneaker.setFitWidth(820);
        image1Sneaker.setFitHeight(500);
        AnchorPane.setTopAnchor(boutonLikes, 10.0);
        AnchorPane.setLeftAnchor(boutonLikes, 10.0);
        anchorPane.getChildren().addAll(image1Sneaker, boutonLikes);
        HBox hboxImagesSneaker = new HBox(20);
        for (int i = 1; i < 3; i++) {
            ImageView imageSupplementaireSneaker = new ImageView(new Image("file:." + sneaker.getImageSneaker(i)));
            imageSupplementaireSneaker.setFitWidth(400);
            imageSupplementaireSneaker.setFitHeight(250);
            hboxImagesSneaker.getChildren().add(imageSupplementaireSneaker);
        }
        Separator separator = new Separator(); // un séparateur pour l'esthétique uniquement
        separator.setStyle("-fx-background-color: black");
        separator.setPrefHeight(4); // épaisseur du trait en pixels
        separator.setPrefWidth(600);
        TitledPane titledPaneDescription;
        List<String> lesDescriptionsSneaker = new ArrayList<>();
        try {
            lesDescriptionsSneaker = this.sneakerSQL.getDescriptionsSneaker(sneaker.getIdSneaker());
        } catch (SneakerInvalideException | SQLException e) {
            System.out.println(e);
        }
        if (!lesDescriptionsSneaker.isEmpty()) { // s'il y a au moins 1 description pour la sneaker
            titledPaneDescription = this.creeTitledPanePageSneaker("Description", lesDescriptionsSneaker);
        } else {
            titledPaneDescription = this.creeTitledPanePageSneaker("Description",
                    List.of("Aucune description pour la sneaker"));
        }
        vboxPartieGauchePageSneaker.getChildren().addAll(anchorPane, hboxImagesSneaker, separator,
                titledPaneDescription);
        VBox.setMargin(separator, new Insets(50, 50, 0, 50));
        VBox.setMargin(titledPaneDescription, new Insets(0, 50, 0, 50));
        return vboxPartieGauchePageSneaker;
    }

    /**
     * Créé partie droite de la page Sneaker
     * 
     * @param sneaker : la sneaker choisie
     * @return VBox : la vbox contenant la partie droite de la page Sneaker
     */
    public VBox creePartieDroitePageSneaker(Sneaker sneaker) {
        VBox vboxPartieDroitePageSneaker = new VBox(20);
        vboxPartieDroitePageSneaker.setAlignment(Pos.TOP_LEFT);
        Text textNomSneaker = new Text(sneaker.getNomSneaker());
        textNomSneaker.setFont(this.getPolicePersonnalisee(35));
        textNomSneaker.setWrappingWidth(500);
        Text textPrixSneaker = new Text("Prix : " + sneaker.getPrix() + " euros");
        textPrixSneaker.setFont(this.getPolicePersonnalisee(30));
        textPrixSneaker.setFill(Color.web("#8787DE"));
        TitledPane titledPaneTaille = this.creeTitledPaneSelectionnerTaille(sneaker);
        TitledPane titledPaneLivraisonRetour = this.creeTitledPanePageSneaker("Livraison & Retour", List.of(
                "Transporteur : Chronopost", "Modes de livraison : à domicile ou en point relais contre signature.",
                "Frais de port : gratuit en point relais pour toute commande supérieure à 200 euros.",
                "Délai de livraison :\n- 2 jours ouvrables pour les produits \"48h Express Delivery\".\n- De 3 à 10 jours ouvrables pour les produits \"Standard Delivery\".",
                "Retours :\nVous devez effectuer votre demande de retour via notre site dans les 14 jours calendaires suivant la réception de votre commande."));
        TitledPane titledPaneMoyensPaiement = this.creeTitledPanePageSneaker("Moyens de paiement", List.of(
                "Nous proposons de nombreuses options de paiement.",
                "Nous acceptons les paiements par carte de crédit (Visa, MasterCard, American Express ou carte de débit pour les paiements étrangers), Apple Pay et PayPal.",
                "Il est également possible de choisir l'option de paiement en 2, 3 ou 4 fois pour une commande d'un montant de 100€ minimum.",
                "Il suffit de sélectionner l'option Alma - Paiement en 2, 3 ou 4 fois pour régler votre achat et ainsi étaler le paiement de façon sécurisée. Votre commande est prise en compte et traitée dans nos délais habituels de 7 jours ouvrés en moyenne (hors week-end et jours fériés). L'option de paiement Alma implique des frais de 2.25% du total de la commande."));
        vboxPartieDroitePageSneaker.getChildren().addAll(textNomSneaker, textPrixSneaker, titledPaneTaille,
                titledPaneLivraisonRetour, titledPaneMoyensPaiement);
        VBox.setMargin(textPrixSneaker, new Insets(0, 0, 50, 0));
        return vboxPartieDroitePageSneaker;
    }

    /**
     * Créé une VBox contenant le titledPane avec les infos des paramètres
     * 
     * @param titreTitledPane : le titre du TitledPane
     * @param lesTextes       : la liste de textes dans le TitledPane
     * @return VBox : la vbox contenant le TitledPane
     */
    public TitledPane creeTitledPanePageSneaker(String titreTitledPane, List<String> lesTextes) {
        VBox vboxTexts = new VBox(50);
        TitledPane titledPane = new TitledPane(titreTitledPane, vboxTexts);
        titledPane.setMaxWidth(700);
        titledPane.setExpanded(false); // définit le TitledPane comme fermé par défaut
        titledPane.setFont(this.getPolicePersonnalisee(30));
        for (String texteActuel : lesTextes) {
            Text text = new Text(texteActuel);
            text.setWrappingWidth(600);
            text.setFont(this.getPolicePersonnalisee(20));
            vboxTexts.getChildren().add(text);
        }
        return titledPane;
    }

    /**
     * Créé une VBox contenant le titledPane pour sélectionner une taille
     * 
     * @param sneaker : la sneaker actuelle
     * @return VBox : la vbox contenant le TitledPane pour sélectionner une taille
     */
    public TitledPane creeTitledPaneSelectionnerTaille(Sneaker sneaker) {
        VBox vboxInfos = new VBox(10);
        vboxInfos.setAlignment(Pos.CENTER);
        vboxInfos.setStyle("-fx-background-color : #8787DE; -fx-background-radius: 15");
        // créé un conteneur intermédiaire pour mettre des margins sur la VBox
        HBox container = new HBox();
        container.setAlignment(Pos.CENTER);
        container.getChildren().add(vboxInfos);
        TitledPane titledPane = new TitledPane("Sélectionner une taille", container);
        titledPane.setMaxWidth(700);
        titledPane.setExpanded(false); // définit le TitledPane comme fermé par défaut
        titledPane.setFont(this.getPolicePersonnalisee(30));

        // gridPane pour les tailles
        GridPane gridPaneTailles = new GridPane();
        gridPaneTailles.setAlignment(Pos.CENTER);
        gridPaneTailles.setHgap(10);
        gridPaneTailles.setVgap(10);
        List<Integer> lesTailles = new ArrayList<>();
        Map<Integer, Integer> lesStocksSneaker = new HashMap<Integer, Integer>();
        try {
            lesTailles = this.sneakerSQL.getLesTailles();
            lesStocksSneaker = this.sneakerSQL.getStocksSneakers(sneaker.getIdSneaker());
            sneaker.rempliStocks(lesTailles, lesStocksSneaker);
        } catch (SneakerInvalideException | SQLException e) {
            System.out.println(e);
        }
        int cpt_taille = 0;
        boolean aucune_taille = true;
        for (Map.Entry<Integer, Integer> valeurs : sneaker.getStocks().entrySet()) {
            Button boutonTaille = new Button("" + valeurs.getKey());
            boutonTaille.setId("blanc");
            this.lesElementsGraphiques.put(boutonTaille.getText(), boutonTaille);
            boutonTaille.setOnAction(new ControleurChoisirTaille(this, lesTailles));
            boutonTaille.setContentDisplay(ContentDisplay.CENTER);
            if (valeurs.getValue() > 0) { // s'il y a au moins 1 paire de disponible en cette taille
                boutonTaille.setStyle("-fx-background-color: #FFFFFF; -fx-background-radius: 15");
                aucune_taille = false;
            } else {
                boutonTaille.setStyle("-fx-background-color: #989595; -fx-background-radius: 15");
                boutonTaille.setDisable(true);
            }
            boutonTaille.setFont(this.getPolicePersonnalisee(30));
            gridPaneTailles.add(boutonTaille, cpt_taille % 4, cpt_taille / 4);
            cpt_taille++;
        }

        // bouton "Ajouter au panier"
        Button boutonAjouterAuPanier = new Button("Ajouter au panier");
        boutonAjouterAuPanier.setOnAction(new ControleurAjouterAuPanier(this, lesTailles, sneaker));
        boutonAjouterAuPanier.setPrefWidth(250);
        boutonAjouterAuPanier.setPrefHeight(50);
        this.creeHoverBouton(boutonAjouterAuPanier, "#F1C22B", "#B79623");
        boutonAjouterAuPanier.setTextFill(Color.BLACK);
        boutonAjouterAuPanier.setFont(this.getPolicePersonnalisee(25));
        if (aucune_taille) {
            Text textAucuneTaille = new Text("Aucune taille disponible !");
            textAucuneTaille.setFont(this.getPolicePersonnalisee(30));
            textAucuneTaille.setFill(Color.RED);
            vboxInfos.getChildren().addAll(gridPaneTailles, textAucuneTaille, boutonAjouterAuPanier);
        } else {
            vboxInfos.getChildren().addAll(gridPaneTailles, boutonAjouterAuPanier);
        }
        VBox.setMargin(gridPaneTailles, new Insets(30, 30, 0, 30));
        VBox.setMargin(boutonAjouterAuPanier, new Insets(50, 30, 30, 30));
        return titledPane;
    }

    /**
     * change la fenetre pour la page de Recherche (Après avoir rentrer des infos)
     * 
     * @param lesSneakers listes des ventes à afficher
     * @param recherche   : le mot rentré dans la barre de recherche
     */
    public void modeRecherche(List<Sneaker> lesSneakers, String recherche) {
        // réinitialisation de la fenetre
        this.panelCentral = new BorderPane();
        this.fenetre.setBackground(this.getBackgroundAutresPages());

        // Text (top du panelCentral)
        Text textAccueil = new Text("Aucun résultat pour la recherche " + recherche);
        if (lesSneakers.size() > 0) { // si la recherche a donné au moins 1 résultat
            textAccueil.setText("Voici les résultats de la recherche " + recherche);
        }
        textAccueil.setFont(this.getPolicePersonnalisee(60));
        textAccueil.setFill(Color.BLACK);
        textAccueil.setUnderline(true);
        BorderPane.setAlignment(textAccueil, Pos.CENTER);
        BorderPane.setMargin(textAccueil, new Insets(50, 0, 50, 0));
        this.panelCentral.setTop(textAccueil);

        // GridPane (center de la panelCentral)
        this.panelCentral.setCenter(this.creerSneakersGridPane(lesSneakers));
        this.fenetre.setTop(this.titreAutresPages());
        this.fenetre.setCenter(this.panelCentral);
    }

    /**
     * Retourne le dictionnaire (Map) des éléments graphiques des fenetres
     * 
     * @return Map<String, Node> : le dictionnaire avec comme clés un String (le nom
     *         de l'élément) et comme valeurs l'élément graphique (Node)
     */
    public Map<String, Node> getLesElementsGraphiques() {
        return this.lesElementsGraphiques;
    }

    /**
     * Retourne l'utilisateur connecté
     * 
     * @return User : l'utilisateur connecté
     */
    public User getUtilisateurConnecte() {
        return this.utilisateurConnecte;
    }

    /**
     * Change l'utilisateur connecté
     * 
     * @param nomUtilisateur le nouvel utilisateur connecté
     */
    public void setUtilisateurConnecte(String nomUtilisateur) {
        try {
            this.utilisateurConnecte = this.userSQL.getUtilisateur(nomUtilisateur);
        } catch (UtilisateurInvalideException e) {
            System.out.println(e);
        }
    }

    /**
     * retourne la police de l'application avec la taille souhaitée
     * 
     * @param tailleTexte : la taille de police souhaitée
     * @return Font : la police de l'application avec la taille souhaitée
     */
    public Font getPolicePersonnalisee(int tailleTexte) {
        String fontPath = "../font/KeaniaOne-Regular.ttf";
        return Font.loadFont(getClass().getResourceAsStream(fontPath), tailleTexte);
    }

    /**
     * Retourne le background de la page Connexion et la page Inscription
     * 
     * @return
     */
    public Background getBackgroundConnexionInscription() {
        Image imageBackground = new Image("file:./img/bg.png");
        BackgroundImage backgroundImage = new BackgroundImage(imageBackground, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        return new Background(backgroundImage);
    }

    /**
     * Retourne le background des autres pages
     * 
     * @return
     */
    public Background getBackgroundAutresPages() {
        Image imageBackground = new Image("file:./img/bg_2.png");
        BackgroundImage backgroundImage = new BackgroundImage(imageBackground, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        return new Background(backgroundImage);
    }

    /**
     * Change la couleur du bouton lorsqu'on survol dessus
     * 
     * @param bouton         : le bouton où l'on veut mettre le hover
     * @param couleurDeBase  : la couleur de base lorsqu'on ne survole pas le bouton
     * @param couleurEnHover : la couleur lorsqu'on survol le bouton
     */
    public void creeHoverBouton(Button bouton, String couleurDeBase, String couleurEnHover) {
        bouton.setStyle("-fx-background-color: " + couleurDeBase + "; -fx-background-radius: 15");
        bouton.setOnMouseEntered(
                e -> bouton.setStyle("-fx-background-color: " + couleurEnHover + "; -fx-background-radius: 15")); // style
                                                                                                                  // lors
                                                                                                                  // du
                                                                                                                  // survol
        bouton.setOnMouseExited(
                e -> bouton.setStyle("-fx-background-color: " + couleurDeBase + "; -fx-background-radius: 15")); // réinitialisation
                                                                                                                 // du
                                                                                                                 // style
    }

    /**
     * Savoir si le mail passé en paramètre est valide (respecte les
     * caractéristiques d'une vraie adresse mail)
     * 
     * @param email : l'email à tester
     * @return true si le mail est valide, sinon false
     */
    public boolean mailValide(String email) {
        String regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
        return email.matches(regex);
    }

    /**
     * Savoir si le mot de passe passé en paramètre est valide (respecte les
     * caractéristiques d'un vrai mot de passe)
     * 
     * @param mdp : le mot de passe à tester
     * @return true si le mot de passe est valide, sinon false
     */
    public boolean mdpValide(String mdp) {
        // vérifier les conditions de validation expliquées dans la pop-up
        if (mdp.length() < 8 || !mdp.matches(".*[A-Z].*") || !mdp.matches(".*[a-z].*") || !mdp.matches(".*\\d.*")
                || !mdp.matches(".*[@#$%^&+=].*")) {
            return false;
        }
        // le mot de passe satisfait toutes les conditions de validation
        return true;
    }

    /**
     * Change infos de l'utilisateur connecté si elles sont différentes
     * 
     * @param pseudo String : le pseudo de l'utilisateur connecté
     * @param email  String : le mail de l'utilisateur connecté
     * @param mdp    String : le mot de passe de l'utilisateur connecté
     */
    public void changeInfosUser(String pseudo, String email, String mdp) {
        if (mailValide(email) && mdpValide(mdp)) {// si le nouveau mail et le nouveau mot de passe sont valides
            if (!pseudo.equals(this.utilisateurConnecte.getPseudo())) {
                try {
                    this.userSQL.updatePseudo(this.utilisateurConnecte.getIdUser(), pseudo);
                } catch (PseudoException e) {
                    this.popUpPersonnaliseeErreur("Pseudo déjà utilisé", "Procédure stoppée",
                            "Nouveau pseudo déjà utilisé").showAndWait();
                    return; // quitte la méthode pour ne changer aucune information car le nouveau pseudo
                            // est déjà utilisé
                } catch (UtilisateurInvalideException e) {
                    System.out.println(e);
                } catch (SQLException e) {
                    System.out.println(e);
                }
                this.utilisateurConnecte.setPseudo(pseudo);
            }
            if (!email.equals(this.utilisateurConnecte.getEmail())) {
                try {
                    this.userSQL.updateMail(this.utilisateurConnecte.getIdUser(), email);
                } catch (UtilisateurInvalideException e) {
                    System.out.println(e);
                } catch (SQLException e) {
                    System.out.println(e);
                }
                this.utilisateurConnecte.setEmail(email);
            }
            if (!mdp.equals("")) {
                try {
                    this.userSQL.updateMotDePasse(this.utilisateurConnecte.getIdUser(), mdp);
                } catch (UtilisateurInvalideException e) {
                    System.out.println(e);
                } catch (SQLException e) {
                    System.out.println(e);
                }
                this.utilisateurConnecte.setPassword(mdp);
            }
        } else if (mailValide(email) && !mdpValide(mdp)) { // mdp uniquement invalide
            this.popUpPersonnaliseeErreur(
                    "Votre mot de passe entré est invalide\n\nUn mot de passe doit avoir :\n-> une longueur minimale de 8 caractères\n\n-> au moins une lettre majuscule\n\n-> au moins une lettre minuscule\n\n-> au moins un chiffre\n\n-> au moins un caractère spécial '@#$%^&+='",
                    "Procédure stoppée", "Mot de passe invalide").showAndWait();
        } else if (mdpValide(mdp) && !mailValide(email)) { // mail uniquement invalide
            this.popUpPersonnaliseeErreur(
                    "Votre adresse mail est invalide\n\nUne adresse mail doit contenir :\n-> au moins un caractère alphabétique, numérique, ou l'un des caractères spéciaux suivants : '._%+-'\n\n-> le symbole '@'\n\n-> au moins un caractère alphabétique, numérique, ou le caractère spécial '-', avant le symbole '@'\n\n-> le symbole '.'\n\n-> au moins deux caractères alphabétiques après le '.'",
                    "Procédure stoppée", "Adresse mail entrée invalide").showAndWait();
        } else { // mdp et mail invalide
            this.popUpPersonnaliseeErreur(
                    "Votre adresse mail et votre mot de passe sont invalides\n\nUne adresse mail doit contenir :\n-> au moins un caractère alphabétique, numérique, ou l'un des caractères spéciaux suivants : '._%+-'\n\n-> le symbole '@'\n\n-> au moins un caractère alphabétique, numérique, ou le caractère spécial '-', avant le symbole '@'\n\n-> le symbole '.'\n\n-> au moins deux caractères alphabétiques après le '.'\n\n\nUn mot de passe doit avoir :\n-> une longueur minimale de 8 caractères\n\n-> au moins une lettre majuscule\n\n-> au moins une lettre minuscule\n\n-> au moins un chiffre\n\n-> au moins un caractère spécial '@#$%^&+='",
                    "Procédure stoppée", "Adresse mail et mot de passe entrés invalides").showAndWait();
        }
    }

    /**
     * Change la couleur du logo de favori de la sneaker cliquée
     * 
     * @param nomImage : le nom du fichier de l'image à charger en conséquence
     */
    public void changeImageFavori(String nomImage) {
        Button boutonFavori = (Button) lesElementsGraphiques.get("boutonLikes");
        ImageView nouvelleImageLike = new ImageView(new Image("file:./img/" + nomImage));
        nouvelleImageLike.setFitWidth(30);
        nouvelleImageLike.setFitHeight(25);
        boutonFavori.setGraphic(nouvelleImageLike);
    }

    /**
     * alerte (pop-up) pour la déconnexion
     * 
     * @return Alert la pop-up de la déconnexion
     */
    public Alert popUpConnecte() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Vous êtes connecté!\n Etes-vous sûr de vous déconnecter ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Déconnexion voulue");
        alert.setHeaderText("Vous allez vous déconnecter");
        return alert;
    }

    /**
     * alerte (pop-up) pour valider le changement d'informations
     * 
     * @return Alert la pop-up pour valider le changement d'informations
     */
    public Alert popUpChangerInfos() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Êtes-vous sûr de changer vos informations ?",
                ButtonType.YES, ButtonType.NO);
        alert.setTitle("Changement de vos informations");
        alert.setHeaderText("Vous allez changer vos informations");
        return alert;
    }

    /**
     * alerte (pop-up) pour le changement d'une image trop grande
     * 
     * @return Alert la pop-up du changement d'une image trop grande
     */
    public Alert popUpImageTropGrande() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Image chargée trop grande ! Réessayez");
        alert.setTitle("Changement d'image impossible");
        alert.setHeaderText("Image trop grande (maximum accepté : 16Mo)");
        return alert;
    }

    /**
     * alerte (pop-up) personnalisée lors d'une information
     * 
     * @param message          : le message à afficher
     * @param titre            : le titre de la pop-up
     * @param contenuPrincipal : le contenu principal de la pop-up
     * @return Alert : la pop-up personnalisée lors d'une information
     */
    public Alert popUpPersonnaliseeInformation(String message, String titre, String contenuPrincipal) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message);
        alert.setTitle(titre);
        alert.setHeaderText(contenuPrincipal);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.getDialogPane().setMinWidth(Region.USE_PREF_SIZE);
        return alert;
    }

    /**
     * alerte (pop-up) personnalisée lors d'une erreur
     * 
     * @param message          : le message à afficher
     * @param titre            : le titre de la pop-up
     * @param contenuPrincipal : le contenu principal de la pop-up
     * @return Alert : la pop-up personnalisée lors d'une erreur
     */
    public Alert popUpPersonnaliseeErreur(String message, String titre, String contenuPrincipal) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message);
        alert.setTitle(titre);
        alert.setHeaderText(contenuPrincipal);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.getDialogPane().setMinWidth(Region.USE_PREF_SIZE);
        return alert;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * 
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("Sneak' Addict - Restez dans l'esprit");
        stage.setScene(this.laScene());
        this.modeConnexion();
        stage.show();
    }

    /**
     * Programme principal
     * 
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }
}
