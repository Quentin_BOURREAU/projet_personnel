import javafx.event.EventHandler;
import java.util.Map;
import java.util.Optional;
import javafx.event.ActionEvent ;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;

/**
 * Controleur de valider les modifications d'informations personnelles
 */
public class ControleurValider implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet : vue  de l'appli
    */
    public ControleurValider(AppliProjet appli){
        this.appli = appli;
    }

    /**
     * Actions à effectuer lors du clic sur le trait vert (valider les modifications d'informations personnelles)
     *
     * @param event l'événement
     */
    @Override
    public void handle(ActionEvent event){
        Optional<ButtonType> reponse = this.appli.popUpChangerInfos().showAndWait(); // on lance la fenêtre popup et on attends la réponse
        // si la réponse est oui
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            Map<String,Node> lesElements = this.appli.getLesElementsGraphiques();
            TextField tf1 = (TextField) lesElements.get("reponseNomUtilisateur");
            TextField tf2 = (TextField) lesElements.get("reponseMail");
            TextField tf3 = (TextField) lesElements.get("reponseMdp");
            this.appli.changeInfosUser(tf1.getText(), tf2.getText(), tf3.getText());
            this.appli.majmodeProfilNonEditable();
        }
    }
}