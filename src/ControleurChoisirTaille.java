import java.util.List;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;

public class ControleurChoisirTaille implements EventHandler<ActionEvent>{
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    /**
    * la liste des tailles existantes pour une sneaker
    */
    private List<Integer> lesTailles;
    
    /**
    * @param AppliProjet : vue  de l'appli
    * @param lesTailles : la liste des tailles existantes pour une sneaker
    */
    public ControleurChoisirTaille(AppliProjet appli, List<Integer> lesTailles){
        this.appli = appli;
        this.lesTailles = lesTailles;
    }
    
    /**
     * Actions à effectuer lors du clic sur une taille pour une sneaker
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        Button boutonClique = (Button) event.getSource();
        String nomBouton = boutonClique.getText();
        Map<String, Node> lesElementsGraphiques = this.appli.getLesElementsGraphiques();
        Button boutonAModifie = (Button) lesElementsGraphiques.get(nomBouton);
        if (boutonAModifie.getId().equals("blanc")){
            boolean boutonCliquable = true;
            for (Integer tailleActuelle : this.lesTailles){
                Button boutonTailleActuelle = (Button) lesElementsGraphiques.get(""+tailleActuelle);
                if (boutonTailleActuelle.getId().equals("rouge")){
                    boutonCliquable = false;
                }
            }
            if (boutonCliquable){
                boutonAModifie.setStyle("-fx-background-color : red; -fx-background-radius : 15");
                boutonAModifie.setId("rouge");
            }
        }
        else{
            boutonAModifie.setStyle("-fx-background-color : white; -fx-background-radius : 15");
            boutonAModifie.setId("blanc");
        }
    }
}
