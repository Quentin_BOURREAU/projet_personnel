import javafx.event.EventHandler;
import javafx.event.ActionEvent ;
import java.io.FileNotFoundException;
import java.util.Map;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * Controleur du bouton inscription
 */
public class ControleurInscription implements EventHandler<ActionEvent>{
    
    /**
    * Vue de l'appli
    */
    private AppliProjet appli;
    private UserSQL user;    
    /**
    * @param AppliProjet vue  de l'appli
    */
    public ControleurInscription(AppliProjet appli){
        this.appli = appli;
        this.user = new UserSQL();
    }

    /**
     * Savoir si le mail passé en paramètre est valide (respecte les caractéristiques d'une vraie adresse mail)
     * @param email : l'email à tester
     * @return true si le mail est valide, sinon false
     */
    public boolean mailValide(String email) {
        String regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
        return email.matches(regex);
    }

    /**
     * Savoir si le mot de passe passé en paramètre est valide (respecte les caractéristiques d'un vrai mot de passe)
     * @param mdp : le mot de passe à tester
     * @return true si le mot de passe est valide, sinon false
     */
    public boolean mdpValide(String mdp){
        // vérifier les conditions de validation expliquées dans la pop-up
        if (mdp.length() < 8 || !mdp.matches(".*[A-Z].*") || !mdp.matches(".*[a-z].*") || !mdp.matches(".*\\d.*") || !mdp.matches(".*[@#$%^&+=].*")) {
            return false;
        }
        // le mot de passe satisfait toutes les conditions de validation
        return true;
    }
  
    /**
     * Actions à effectuer lors du clic sur le bouton inscription
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        Button button = (Button) (event.getSource());
        Map<String, Node> map = this.appli.getLesElementsGraphiques();
        TextField tfNom = (TextField) map.get("tfNomUtilisateur");
        TextField tfMail = (TextField) map.get("tfMail");
        PasswordField passwdf = (PasswordField) map.get("passwdf");
        PasswordField passwdfConf = (PasswordField) map.get("passwdConfirmation");
        if (button.getText().contains("Je n'ai pas de compte")){
            this.appli.modeInscription();
        }
        else{
            if (button.getText().contains("Inscription")){
                if (!this.user.getLesPseudosUtilisateurs().contains(tfNom.getText()) && !tfNom.getText().equals("")){ // si le pseudo n'est pas déjà pris (soit qu'il n'existe pas dans la base de données)
                    String mailEntre = tfMail.getText();
                    if (this.mailValide(mailEntre)){ // si le mail est valide (respecte les conditions d'une adresse mail)
                        String passwd = passwdf.getText();
                        String passwdConfirmation = passwdfConf.getText();
                        if (!passwd.equals("") && !passwdConfirmation.equals("") && passwd.equals(passwdConfirmation)) { // mot de passe et confirmation identiques et pas vide
                            if (this.mdpValide(passwd)){ // si le mot de passe est valide (respecte les caractéristiques d'un mot de passe)
                                try{
                                    this.user.ajouteUser(tfNom.getText(), mailEntre, passwd); // crée l'utilisateur dans la base de données
                                    this.appli.modeConnexion();
                                }
                                catch (FileNotFoundException e){
                                    System.out.println(e);
                                }
                            }
                            else{
                                this.appli.popUpPersonnaliseeErreur("Votre mot de passe entré est invalide\n\nUn mot de passe doit avoir :\n-> une longueur minimale de 8 caractères\n\n-> au moins une lettre majuscule\n\n-> au moins une lettre minuscule\n\n-> au moins un chiffre\n\n-> au moins un caractère spécial '@#$%^&+='", "Procédure stoppée", "Mot de passe invalide").showAndWait();
                            }
                        }
                        else {
                            this.appli.popUpPersonnaliseeErreur("Mots de passes entrés non-correspondants !", "Procédure stoppée", "Mots de passes non-correpondants").showAndWait();
                        }
                    }
                    else{ // le mail n'est pas valide
                        this.appli.popUpPersonnaliseeErreur("Votre adresse mail est invalide\n\nUne adresse mail doit contenir :\n-> au moins un caractère alphabétique, numérique, ou l'un des caractères spéciaux suivants : '._%+-'\n\n-> le symbole '@'\n\n-> au moins un caractère alphabétique, numérique, ou le caractère spécial '-', avant le symbole '@'\n\n-> le symbole '.'\n\n-> au moins deux caractères alphabétiques après le '.'", "Procédure stoppée", "Adresse mail entrée invalide").showAndWait();
                    }
                }
                else if (tfNom.getText().equals("")){ // pseudo vide
                        this.appli.popUpPersonnaliseeErreur("Information sur le pseudo manquante", "Procédure stoppée", "Pseudo non-entré").showAndWait();
                }
                else{
                    this.appli.popUpPersonnaliseeErreur("Pseudo déjà utilisé", "Procédure stoppée", "Pseudo entré déjà utilisé").showAndWait();
                }
            }
        }
    } 
}