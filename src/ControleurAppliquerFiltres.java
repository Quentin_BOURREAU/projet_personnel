import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurAppliquerFiltres implements EventHandler<ActionEvent>{

    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet vue  de l'appli
    */
    public ControleurAppliquerFiltres(AppliProjet appli){
        this.appli = appli;
    }

    /**
     * Actions à effectuer lors du clic le bouton appliquer (pour les filtres)
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        // a implémenter
    }   
}
