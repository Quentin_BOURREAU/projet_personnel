import java.sql.*;

/**Connexion à la base de donnée */
public class Connexion {
    public static Connection laConnexion = Connexion.connect();
    /**
     * Connexion
     * @return la connexion à la base de donnée
     * @throws DBNotFundException la base de donnée n'est pas trouvée
     */
    public static Connection connect(){
		try{
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/"+"PROJET","quentin","quentin");
        }
        catch(SQLException e){
            return null;
        }
	}
}
