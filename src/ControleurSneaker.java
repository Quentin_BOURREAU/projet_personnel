import javafx.event.EventHandler;
import javafx.event.ActionEvent;

/**
 * Controleur des sneakers 
 */
public class ControleurSneaker implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    /**
     * La sneaker du controleur
     */
    private Sneaker sneaker;
    
    /**
    * @param AppliProjet : vue de l'appli
    */
    public ControleurSneaker(AppliProjet appli,Sneaker sneaker){
        this.appli = appli;
        this.sneaker = sneaker;
    }
     
    /**
     * Actions à effectuer lors du clic sur le bouton d'une sneaker 
     * @param event l'événement
     */    
    @Override
    public void handle(ActionEvent event){
        this.appli.modeSneaker(this.sneaker);
    } 
}