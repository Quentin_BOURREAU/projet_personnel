import javafx.event.EventHandler;
import javafx.scene.control.Button;

import java.sql.SQLException;

import javafx.event.ActionEvent;

/**
 * Controleur des sneakers 
 */
public class ControleurSupprimerSneakerPanier implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    /**
     * La sneaker du controleur
     */
    private Sneaker sneaker;
    /**
     * La taille souhaitée de la sneaker
     */
    private int taille;
    
    /**
    * @param AppliProjet : vue de l'appli
    */
    public ControleurSupprimerSneakerPanier(AppliProjet appli, Sneaker sneaker, int taille){
        this.appli = appli;
        this.sneaker = sneaker;
        this.taille = taille;
    }
     
    /**
     * Actions à effectuer lors du clic sur le bouton d'une sneaker 
     * @param event l'événement
     */    
    @Override
    public void handle(ActionEvent event){
        SneakerSQL sneakerSQL = new SneakerSQL();
        int idut = this.appli.getUtilisateurConnecte().getIdUser();
        int idsneaker = this.sneaker.getIdSneaker();
        try{
            sneakerSQL.enleveAchat(idut, this.taille, idsneaker);
        }
        catch (SQLException e){System.out.println(e);}
        this.appli.modePanier();
    } 
}