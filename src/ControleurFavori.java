import javafx.event.EventHandler;
import javafx.event.ActionEvent ;

/**
 * Controleur des likes
 */
public class ControleurFavori implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet : vue  de l'appli
    */
    public ControleurFavori(AppliProjet appli){
        this.appli = appli;
    }
    
    /**
     * Actions à effectuer lors du clic sur les likes 
     * @param event l'événement
     */    
    @Override
    public void handle(ActionEvent event){
        this.appli.modeFavori();
    } 
}