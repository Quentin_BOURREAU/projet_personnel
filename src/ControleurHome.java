import javafx.event.EventHandler;
import javafx.event.ActionEvent ;

/**
 * Controleur de home
 */
public class ControleurHome implements EventHandler<ActionEvent>{

    /**
     * vue de l'appli
     */
    private AppliProjet appli;
    
    /**
    * @param AppliProjet : vue  de l'appli
    */
    public ControleurHome(AppliProjet appli){
        this.appli = appli;
    }

    /**
     * Actions à effectuer lors du clic sur le bouton home
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        this.appli.modeAccueil();
    }
}
