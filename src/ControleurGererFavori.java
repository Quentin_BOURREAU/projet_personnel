import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.Node;
import java.sql.SQLException;
import java.util.Map;
import javafx.event.ActionEvent;

/**
 * Controleur des images des likes
 */
public class ControleurGererFavori implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    /**
     * La sneaker du controleur
     * */
    private Sneaker sneaker;
    
    /**
    * @param appliProjet : vue  de l'appli
    * @param sneaker : la sneaker à savoir si elle est en favori
    */
    public ControleurGererFavori(AppliProjet appli, Sneaker sneaker){
        this.appli = appli;
        this.sneaker = sneaker;
    }
    
    /**
     * Actions à effectuer lors du clic sur les likes 
     * @param event l'événement
     */    
    @Override
    public void handle(ActionEvent event){
        Map<String, Node> lesElementsGraphiques = this.appli.getLesElementsGraphiques();
        Button boutonLike = (Button) lesElementsGraphiques.get("boutonLikes");
        SneakerSQL sneakerSQL = new SneakerSQL();
        if (boutonLike.getId().equals("noir")){
            try{
                sneakerSQL.ajouteFavori(this.appli.getUtilisateurConnecte().getIdUser(), this.sneaker.getIdSneaker());
                this.appli.changeImageFavori("like_red.png");
                boutonLike.setId("rouge");
            }
            catch (UtilisateurInvalideException e){System.out.println(e);}
            catch (SQLException e){System.out.println(e);}
        }
        else{
            try{
                sneakerSQL.enleveFavori(this.appli.getUtilisateurConnecte().getIdUser(), this.sneaker.getIdSneaker());
                this.appli.changeImageFavori("like_black.png");
                boutonLike.setId("noir");
            }
            catch (UtilisateurInvalideException e){System.out.println(e);}
            catch (SQLException e){System.out.println(e);}
        }
        Button boutonFavoris = (Button) lesElementsGraphiques.get("boutonFavoris");
        int nombreSneakersFavoris = sneakerSQL.sneakersFavori(this.appli.getUtilisateurConnecte().getPseudo()).size(); // récupère le nombre de sneakers en favori de l'utilisateur connecté
        boutonFavoris.setText(nombreSneakersFavoris+"");
        if (nombreSneakersFavoris <= 0){ // si l'utilisateur connecté à au moins 1 sneaker en favori
            boutonFavoris.setText("");
        }
        boutonFavoris.setContentDisplay(ContentDisplay.CENTER);
    } 
}