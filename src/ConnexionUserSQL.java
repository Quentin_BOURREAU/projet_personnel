import java.sql.*;

/** Connexion de Utilisateur */
public class ConnexionUserSQL{
    /** connexion à la base de donnée */
    private Connection connexion = Connexion.laConnexion;

    /** Contructeur */
    public ConnexionUserSQL(){
    }

    /**
     * Verification des informations pour connecter le joueur
     * @param pseudo username
     * @param pw password
     * @return true si le nom d'utilisateur et le mot de passe correspondent dans la base de données
     * @throws MdpUserInvalideException les informations données sont erronées 
     * @throws UtilisateurDesactiveException si l'utilisateur est désactivé
     */
    public boolean userConnexion(String pseudo, String pw) throws MdpUserInvalideException, UtilisateurDesactiveException{
        try{
            PreparedStatement ps = this.connexion.prepareStatement("SELECT * FROM UTILISATEUR WHERE pseudout = ?");
            ps.setString(1, pseudo);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                if(rs.getString("activeut").equals("O")){
                    if(rs.getString(4).equals(pw)){
                        return true;
                    }
                    else{
                        throw new MdpUserInvalideException();
                    }
                }
                else{
                    throw new UtilisateurDesactiveException();
                }
            }
        }
        catch(SQLException e){
            return false;
        }
        return false;
    }
}