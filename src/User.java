public class User {
    /** id de l'utilisateur */
    private int idUser;
    /** pseudonyme de l'utilisateur */
    private String pseudo;
    /** email de l'utilisateur */
    private String email;
    /** mot de passe de l'utilisateur */
    private String password;
    /** id du role de l'utilisateur */
    private int idRole;
    /** photo de l'utilisateur */
    private String photoProfil;

    /**
     * Contructeur de User
     * @param idUser id de l'utilisateur
     * @param pseudo pseudonyme de l'utilisateur 
     * @param email email de l'utilisateur
     * @param password mot de passe de l'utilisateur
     * @param photoProfile photo de l'utilisateur
     */
    public User(int idUser, String pseudo, String email, String password,int idRole, String photoProfil) {
        this.idUser = idUser;
        this.pseudo = pseudo;
        this.email = email;
        this.password = password;
        this.idRole = idRole;
        if(photoProfil == null){
            this.photoProfil = "/img/null.png";
        }
        else{
            this.photoProfil = photoProfil;
        }
    }

    /**
     * getter de idUser
     * @return idUser
    */
    public int getIdUser() {
        return idUser;
    }

    /**
     * getter de pseudo
     * @return pseudo
    */
    public String getPseudo() {
        return pseudo;
    }

    /**
     * getter de email
     * @return email
    */
    public String getEmail() {
        return email;
    }

    /**
     * getter de password
     * @return password
    */
    public String getPassword() {
        return password;
    } 

    /**
     * getter de idRole
     * @return idRole
    */
    public int getIdRole() {
        return idRole;
    } 

    /**
     * getter de photoProfil
     * @return photoProfil
    */
    public String getphotoProfil() {
        return photoProfil;
    }

    /**
     * setter de pseudo
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /** 
     * setter de email
     * */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * setter du MotDePasse
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * setter de la photo de profil
     */
    public void setPhotoProfil(String cheminNouvellePhoto) {
        this.photoProfil = cheminNouvellePhoto;
    }
}
