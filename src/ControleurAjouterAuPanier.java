import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javafx.event.ActionEvent ;

/**
 * Controleur permettant d'ajouter une sneaker au panier
 */
public class ControleurAjouterAuPanier implements EventHandler<ActionEvent>{
    
    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    /**
    * la liste des tailles existantes pour une sneaker
    */
    private List<Integer> lesTailles;
    /**
    * le modele de l'appli
    */
    private Sneaker sneaker;
    
    /**
    * @param appli : vue  de l'appli
    * @param lesTailles : la liste des tailles existantes pour une sneaker
    * @param sneaker : la sneaker à ajouter (si les conditions sont valides)
    */
    public ControleurAjouterAuPanier(AppliProjet appli, List<Integer> lesTailles, Sneaker sneaker){
        this.appli = appli;
        this.lesTailles = lesTailles;
        this.sneaker = sneaker;
    }

    /**
     * Actions à effectuer lors du clic sur l'ajout d'une sneaker au panier
     *
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        SneakerSQL sneakerSQL = new SneakerSQL();
        Button boutonTailleSelectionne = null;
        Map<String, Node> lesElementsGraphiques = this.appli.getLesElementsGraphiques();
        for (Integer tailleActuelle : this.lesTailles){
            Button boutonTailleActuelle = (Button) lesElementsGraphiques.get(""+tailleActuelle);
            if (boutonTailleActuelle.getId().equals("rouge")){
                boutonTailleSelectionne = boutonTailleActuelle;
            }
        }
        if (boutonTailleSelectionne == null){
            this.appli.popUpPersonnaliseeErreur("Vous n'avez sélectionnez aucune taille pour la sneaker!\n\nVeuillez sélectionner une taille pour l'ajouter au panier", "Taille manquante", "Aucune taille sélectionnée !").showAndWait();
        }
        else{
            try {
                sneakerSQL.ajouteAchat(this.appli.getUtilisateurConnecte().getIdUser(), Integer.parseInt(boutonTailleSelectionne.getText()), this.sneaker.getIdSneaker());
                this.appli.popUpPersonnaliseeInformation("Vous avez ajoutez cette sneaker en taille "+boutonTailleSelectionne.getText()+" au panier", "Sneaker ajoutée au panier", "Ajout de la sneaker au panier !").showAndWait();
                Button boutonPanier = (Button) lesElementsGraphiques.get("boutonPanier");
                Map<List<Integer>,List<Sneaker>> lesSneakersPanier = sneakerSQL.sneakersPanier(this.appli.getUtilisateurConnecte().getPseudo());
                Collection<List<Sneaker>> collectionLesSneakers = lesSneakersPanier.values();
                List<Sneaker> lesSneakers = new ArrayList<>();
                for (List<Sneaker> liste : collectionLesSneakers){
                    lesSneakers = liste;
                }
                int nombreSneakersPanier = lesSneakers.size(); // récupère le nombre de sneakers dans le panier de l'utilisateur connecté
                boutonPanier.setText(nombreSneakersPanier+"");
                if (nombreSneakersPanier <= 0){
                    boutonPanier.setText("");
                }
                boutonPanier.setContentDisplay(ContentDisplay.CENTER);
            }
            catch (SQLException e){
                System.out.println(e);
                // this.appli.popUpPersonnaliseeErreur("Vous avez déjà sélectionnez cette taille pour la sneaker dans votre panier!\n\nVeuillez sélectionner une taille pour l'ajouter au panier", "Taille manquante", "Aucune taille sélectionnée !").showAndWait();
            }
        }
    }
}