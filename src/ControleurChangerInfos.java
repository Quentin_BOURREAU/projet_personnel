import javafx.event.EventHandler;
import javafx.event.ActionEvent ;

/**
 * Controleur de changerInfos
 */
public class ControleurChangerInfos implements EventHandler<ActionEvent>{
    
    /**
    * AppliProjet : vue de l'appli
    */
    private AppliProjet appli;
    
    /**
    * @param AppliVAE vue  de l'appli
    */
    public ControleurChangerInfos(AppliProjet appli){
        this.appli = appli;
    }

    /**
     * Actions à effectuer lors du clic sur le bouton changer mes informations
     * @param event l'événement
     */     
    @Override
    public void handle(ActionEvent event){
        this.appli.majmodeProfilEditable();
    }
}