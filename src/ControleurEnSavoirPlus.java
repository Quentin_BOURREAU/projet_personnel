import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurEnSavoirPlus implements EventHandler<ActionEvent>{

    /**
    * vue de l'appli
    */
    private AppliProjet appli;
    /**
     * La sneaker du controleur
     */
    private SneakerSQL sneakerSQL;
    
    /**
    * @param AppliProjet vue  de l'appli
    */
    public ControleurEnSavoirPlus(AppliProjet appli){
        this.appli = appli;
        this.sneakerSQL = new SneakerSQL();
    }

    /**
     * Actions à effectuer lors du clic le bouton en savoir plus (dans page accueil)
     * @param event l'événement
     */
    @Override
    public void handle(ActionEvent event){
        Button boutonClique = (Button) event.getSource();
        String idBoutonClique = boutonClique.getId();
        try{
            System.out.println("idsneaker : "+ idBoutonClique);
            this.appli.modeSneaker(this.sneakerSQL.getSneaker(Integer.parseInt(idBoutonClique)));
        }
        catch (SneakerInvalideException e){System.out.println(e);}
        catch (SQLException e){System.out.println(e);}
    }   
}
