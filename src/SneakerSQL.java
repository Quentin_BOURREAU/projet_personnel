import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** SneakerSQL */
public class SneakerSQL {
    /** connexion à la base de donnée */
    private Connection connexion = Connexion.laConnexion;

    /** Constructeur de SneakerSQL */
    public SneakerSQL(){
    }

    /**
     * La liste des sneakers proposées
     * @return List<Sneaker> : la liste des sneakers 
     */
    public List<Sneaker> sneakersProposees(){
        List<Sneaker> lesSneakers = new ArrayList<>();
        try{
            PreparedStatement ps = connexion.prepareStatement("SELECT * FROM SNEAKER");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                lesSneakers.add(new Sneaker(rs.getInt("idsneaker"),rs.getString("nomsneaker"),rs.getInt("prix"),rs.getInt("idmarque"),rs.getInt("idsexe"),rs.getInt("idcoloris")));
            }
            return lesSneakers;
        }
        catch(SQLException e){
            return null;
        }
    }

    /**
     * Sneakers au panier d'un utilisateur
     * @param pseudo : le pseudo de l'utilisateur
     * @return Map<List<Integer>, List<Sneaker>> : le dictionnaire avec comme clé la liste des tailles associées aux sneakers dans le panier et comme valeur la liste des sneakers au panier
     */
    public Map<List<Integer>,List<Sneaker>> sneakersPanier(String pseudo){
        Map<List<Integer>, List<Sneaker>> dictSneakersPanier = new HashMap<>();
        List<Integer> lesTaillesSouhaitees = new ArrayList<>();
        List<Sneaker> lesSneakers = new ArrayList<>();
        try{
            PreparedStatement ps = connexion.prepareStatement("SELECT * FROM SNEAKER NATURAL JOIN UTILISATEUR NATURAL JOIN ACHAT NATURAL JOIN TAILLE WHERE pseudout = ?");
            ps.setString(1, pseudo);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                lesTaillesSouhaitees.add(rs.getInt("taille"));
                lesSneakers.add(new Sneaker(rs.getInt("idsneaker"),rs.getString("nomsneaker"),rs.getInt("prix"),rs.getInt("idmarque"),rs.getInt("idsexe"),rs.getInt("idcoloris")));
            }
            dictSneakersPanier.put(lesTaillesSouhaitees, lesSneakers);
            return dictSneakersPanier;
        }
        catch(SQLException e){
            return null;
        }
    }

    /**
     * Sneakers au favorites d'un utilisateur
     * @param pseudo : le pseudo de l'utilisateur
     * @return List<Sneaker> : la liste de ses sneakers favorites
     */
    public List<Sneaker> sneakersFavori(String pseudo){
        List<Sneaker> lesSneakers = new ArrayList<>();
        try{
            PreparedStatement ps = connexion.prepareStatement("SELECT * FROM SNEAKER NATURAL JOIN UTILISATEUR NATURAL JOIN FAVORI WHERE pseudout = ?");
            ps.setString(1, pseudo);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                lesSneakers.add(new Sneaker(rs.getInt("idsneaker"),rs.getString("nomsneaker"),rs.getInt("prix"),rs.getInt("idmarque"),rs.getInt("idsexe"),rs.getInt("idcoloris")));
            }
            return lesSneakers;
        }
        catch(SQLException e){
            return null;
        }
    }

    /**
     * Retourne la liste des sneakers suivant la recherche
     * @param recherche : la lettre ou les lettres de la recherche
     * @return List<Sneaker> : la liste des sneakers correpsondantes à la recherche
     */
    public List<Sneaker> sneakersRecherchees(String recherche){
        try{
            List<Sneaker> lesSneakers = new ArrayList<>();
            PreparedStatement ps = connexion.prepareStatement("SELECT * FROM SNEAKER WHERE nomsneaker LIKE ?");
            ps.setString(1,"%"+recherche+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                lesSneakers.add(new Sneaker(rs.getInt("idsneaker"),rs.getString("nomsneaker"),rs.getInt("prix"),rs.getInt("idmarque"),rs.getInt("idsexe"),rs.getInt("idcoloris")));
            }
            return lesSneakers;
        }
        catch(SQLException e){
            return null;}
    }

    /**
     * récupère la Snealer corresponant à l'id donné
     * @param idsneaker : l'id de la sneaker recherchée
     * @return la Sneaker correspondante
     * @throws SQLException si la requete a eu un problème
     * @throws SneakerInvalideException si la sneaker n'existe pas
     */
    public Sneaker getSneaker(int idsneaker) throws SQLException, SneakerInvalideException {
        PreparedStatement checkSneaker = connexion.prepareStatement("select * from SNEAKER where idsneaker = ?");
        checkSneaker.setInt(1, idsneaker);
        ResultSet rs = checkSneaker.executeQuery();
        if (!rs.next()){
            throw new SneakerInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("SELECT * FROM SNEAKER WHERE idsneaker = ?");
        ps.setInt(1, idsneaker);
        rs = ps.executeQuery();
        if (rs.next()){
            return new Sneaker(rs.getInt("idsneaker"),rs.getString("nomsneaker"),rs.getInt("prix"),rs.getInt("idmarque"),rs.getInt("idsexe"),rs.getInt("idcoloris"));
        }
        else{
            return null;
        }      
    }

    /**
     * Permet de créer une photo pour une sneaker
     * @param idSneaker : l'id de la sneaker
     * @throws SQLException si il y'a une erreur lors de la requete SQL
     * @throws SneakerInvalideException si la sneaker n'existe pas
     * @throws IOException si il y'a une erreur lors de l'écriture du nouveau fichier
     * @throws PhotoManquanteException s'il n'y a pas de photo enregistré pour cette sneaker
     */
    public void getPhoto(int idSneaker) throws SQLException, SneakerInvalideException, IOException, PhotoManquanteException{
        PreparedStatement checkSneaker = connexion.prepareStatement("select idsneaker from SNEAKER where idsneaker = ?");
        checkSneaker.setInt(1, idSneaker);
        ResultSet rs = checkSneaker.executeQuery();
        if (!rs.next()){
            throw new SneakerInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("select imgph from PHOTO where idsneaker = ?");
        ps.setInt(1, idSneaker);
        rs = ps.executeQuery();
        if (rs.next()){ // s'il y a au moins 1 image pour la sneaker
            // on refait la requete car le next() a parcouru la 1ère ligne de la requete
            rs = ps.executeQuery();
            int cpt_image = 0;
            while (cpt_image < 3 && rs.next()){ // parcours de toutes les photos de la sneaker (maximum 3)
                if (rs.getObject("imgph") != null ){
                    byte[] photoBytes = rs.getBytes("imgph");
                    try (OutputStream outputStream = new FileOutputStream("./img/img_" + idSneaker + cpt_image + ".png")) {
                        outputStream.write(photoBytes); 
                    }
                }
                else{
                    throw new PhotoManquanteException();
                }
                cpt_image ++;
            }
        }
        else{
            throw new PhotoManquanteException();
        }
    }

    /**
     * Permet de récupérer le nombre de photos associées pour une sneaker
     * @param idSneaker : l'id de la sneaker
     * @throws SQLException si il y'a une erreur lors de la requete SQL
     * @throws SneakerInvalideException si la sneaker n'existe pas
     */
    public int getNbPhotos(int idSneaker) throws SQLException, SneakerInvalideException{
        PreparedStatement checkSneaker = connexion.prepareStatement("select idsneaker from SNEAKER where idsneaker = ?");
        checkSneaker.setInt(1, idSneaker);
        ResultSet rs = checkSneaker.executeQuery();
        if (!rs.next()){
            throw new SneakerInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("select count(*) nb_photos from PHOTO where idsneaker = ?");
        ps.setInt(1, idSneaker);
        rs = ps.executeQuery();
        rs.next();
        return rs.getInt("nb_photos");
    }

    /**
     *  Retourne toutes les descriptions associées à la sneaker dans la base de données
     * @param idSneaker l'id de la sneaker à mettre en favori
     * @return List<String> : la liste des descriptions de la sneaker
     * @throws SneakerInvalideException si la sneaker n'existe pas
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public List<String> getDescriptionsSneaker(int idSneaker) throws SneakerInvalideException, SQLException{
        List<String> lesDescriptions = new ArrayList<>();
        PreparedStatement checkSneaker = connexion.prepareStatement("select idsneaker from SNEAKER where idsneaker = ?");
        checkSneaker.setInt(1, idSneaker);
        ResultSet rs = checkSneaker.executeQuery();
        if (!rs.next()){
            throw new SneakerInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("select descriptionsneaker from DESCRIPTION where idsneaker = ?");
        ps.setInt(1, idSneaker);
        rs = ps.executeQuery();
        while (rs.next()){ // parcours de toutes les descriptions de la sneaker
            lesDescriptions.add(rs.getString("descriptionsneaker"));
        }
        return lesDescriptions;
    }

    /**
     *  Retourne le dictionnaire des stocks de taille de la sneaker
     * @param idSneaker : l'id de la sneaker à regarder
     * @return Map<Integer, Integer> : le dictionnaire des stocks de taille de la sneaker avec comme clé la taille et en valeur le stock de cette taille
     * @throws SneakerInvalideException si la sneaker n'existe pas
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public Map<Integer, Integer> getStocksSneakers(int idSneaker) throws SneakerInvalideException, SQLException{
        Map<Integer, Integer> lesStocks = new HashMap<Integer, Integer>();
        PreparedStatement checkSneaker = connexion.prepareStatement("select idsneaker from SNEAKER where idsneaker = ?");
        checkSneaker.setInt(1, idSneaker);
        ResultSet rs = checkSneaker.executeQuery();
        if (!rs.next()){
            throw new SneakerInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("select taille,quantite from STOCK natural join TAILLE where idsneaker = ?");
        ps.setInt(1, idSneaker);
        rs = ps.executeQuery();
        while (rs.next()){ // parcours de tous les stocks de la sneaker
            lesStocks.put(rs.getInt("taille"), rs.getInt("quantite"));
        }
        return lesStocks;
    }

    /**
     * Retourne la liste des tailles existantes dans la base de données
     * @return List<Integer> : la liste des tailles
     * @throws SQLException si la requete a eu une erreur
     */
    public List<Integer> getLesTailles() throws SQLException{
        List<Integer> lesTailles = new ArrayList<>();
        PreparedStatement ps = connexion.prepareStatement("select taille from TAILLE");
        ResultSet rs = ps.executeQuery();
        while (rs.next()){ // parcours de tous les stocks de la sneaker
            lesTailles.add(rs.getInt("taille"));
        }
        return lesTailles;
    }

    /**
     *  Permet d'ajouter en favori une sneaker à un utilisateur dans la base de données
     * @param idUt l'id de l'utilisateur
     * @param idSneaker l'id de la sneaker à mettre en favori
     * @throws UtilisateurInvalideException si l'utilisateur n'existe pas
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public void ajouteFavori(int idUt, int idSneaker) throws UtilisateurInvalideException, SQLException{
        PreparedStatement checkUser = connexion.prepareStatement("select idut from UTILISATEUR where idut = ?");
        checkUser.setInt(1, idUt);
        ResultSet rs = checkUser.executeQuery();
        if (!rs.next()){
            throw new UtilisateurInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("insert into FAVORI(idut,idsneaker) values (?,?)");
        ps.setInt(1, idUt);
        ps.setInt(2, idSneaker);
        ps.executeUpdate();
    }

    /**
     *  Permet d'enlever en favori une vente à un utilisateur dans la base de données
     * @param idUt l'id de l'utilisateur
     * @param idSneaker l'id de la sneaker à enlever des favoris
     * @throws UtilisateurInvalideException si l'utilisateur n'existe pas
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public void enleveFavori(int idUt, int idSneaker) throws UtilisateurInvalideException, SQLException{
        PreparedStatement checkUser = connexion.prepareStatement("select idut from UTILISATEUR where idut = ?");
        checkUser.setInt(1, idUt);
        ResultSet rs = checkUser.executeQuery();
        if (!rs.next()){
            throw new UtilisateurInvalideException();
        }
        PreparedStatement ps = connexion.prepareStatement("delete from FAVORI where idut = ? and idsneaker = ?");
        ps.setInt(1, idUt);
        ps.setInt(2, idSneaker);
        ps.executeUpdate();
    }

    /**
     *  Permet de mettre au panier une sneaker (avec une taille précise) à un utilisateur dans la base de données
     * @param idUt l'id de l'utilisateur
     * @param taille : la taille souhaitée
     * @param idSneaker l'id de la sneaker à mettre au panier
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public void ajouteAchat(int idUt, int taille, int idSneaker) throws SQLException{
        PreparedStatement ps = connexion.prepareStatement("select idtaille from TAILLE where taille = ?");
        ps.setInt(1, taille);
        ResultSet rs = ps.executeQuery();
        rs.next();
        int idTaille = rs.getInt("idtaille");
        ps = connexion.prepareStatement("insert into ACHAT(idut,idtaille,idsneaker,quantiteachat) values (?,?,?,1)");
        ps.setInt(1, idUt);
        ps.setInt(2, idTaille);
        ps.setInt(3, idSneaker);
        ps.executeUpdate();
    }

    /**
     *  Permet d'enlever au panier une sneaker (avec une taille précise) à un utilisateur dans la base de données
     * @param idUt l'id de l'utilisateur
     * @param taille : la taille souhaitée
     * @param idSneaker l'id de la sneaker à enlever du panier
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public void enleveAchat(int idUt, int taille, int idSneaker) throws SQLException{
        PreparedStatement ps = connexion.prepareStatement("select idtaille from TAILLE where taille = ?");
        ps.setInt(1, taille);
        ResultSet rs = ps.executeQuery();
        rs.next();
        int idTaille = rs.getInt("idtaille");
        ps = connexion.prepareStatement("delete from ACHAT where idut = ? and idtaille = ? and idsneaker = ?");
        ps.setInt(1, idUt);
        ps.setInt(2, idTaille);
        ps.setInt(3, idSneaker);
        ps.executeUpdate();
    }

    /**
     *  Permet de récupérer la quantité de cette sneaker de cette taille souhaitée (mise dans le panier pour l'acheter)
     * @param idUt l'id de l'utilisateur
     * @param taille : la taille souhaitée
     * @param idSneaker l'id de la sneaker à mettre au panier
     * @return int : la quantité de cette sneaker de cette taille ajoutée au panier
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public int getQuantiteSneakerTaille(int idUt, int taille, int idSneaker) throws SQLException{
        PreparedStatement ps = connexion.prepareStatement("select idtaille from TAILLE where taille = ?");
        ps.setInt(1, taille);
        ResultSet rs = ps.executeQuery();
        rs.next();
        int idTaille = rs.getInt("idtaille");
        ps = connexion.prepareStatement("select quantiteachat from ACHAT where idut = ? and idtaille = ? and idsneaker = ?");
        ps.setInt(1, idUt);
        ps.setInt(2, idTaille);
        ps.setInt(3, idSneaker);
        rs = ps.executeQuery();
        rs.next();
        return rs.getInt("quantiteachat");
    }

    /**
     *  Permet de modifier la quantité de cette sneaker de cette taille souhaitée (mise dans le panier pour l'acheter)
     * @param idUt l'id de l'utilisateur
     * @param taille : la taille souhaitée
     * @param idSneaker l'id de la sneaker à mettre au panier
     * @throws SQLException si il y'a une erreur dans les requetes sql
     */
    public void modifieQuantiteSneakerTaille(int idUt, int taille, int idSneaker, int nouvelleQuantite) throws SQLException{
        PreparedStatement ps = connexion.prepareStatement("select idtaille from TAILLE where taille = ?");
        ps.setInt(1, taille);
        ResultSet rs = ps.executeQuery();
        rs.next();
        int idTaille = rs.getInt("idtaille");
        ps = connexion.prepareStatement("update ACHAT set quantiteachat = ? where idut = ? and idtaille = ? and idsneaker = ?");
        ps.setInt(1, nouvelleQuantite);
        ps.setInt(2, idUt);
        ps.setInt(3, idTaille);
        ps.setInt(4, idSneaker);
        ps.executeUpdate();
    }
}