-- partie utilisateurs (utilisateur et administrateur)
CREATE TABLE IF NOT EXISTS ROLE (
idrole DECIMAL(2,0),
nomrole VARCHAR(30),
PRIMARY KEY (idrole)
);

CREATE TABLE IF NOT EXISTS UTILISATEUR (
idut DECIMAL(6,0),
pseudout VARCHAR(20) unique,
emailut VARCHAR(100),
mdput VARCHAR(100),
photo longblob,
activeut char,
idrole DECIMAL(2,0),
PRIMARY KEY (idut),
FOREIGN KEY (idrole) REFERENCES ROLE (idrole)
);

-- partie sneaker (vente)
CREATE TABLE IF NOT EXISTS MARQUE (
  idmarque DECIMAL(4,0),
  nommarque VARCHAR(30),
  PRIMARY KEY (idmarque)
);

CREATE TABLE IF NOT EXISTS SEXE (
  idsexe DECIMAL(2,0),
  nomsexe VARCHAR(30),
  PRIMARY KEY (idsexe)
);

CREATE TABLE IF NOT EXISTS TAILLE (
  idtaille DECIMAL(3,0),
  taille DECIMAL(3,0) UNIQUE,
  PRIMARY KEY (idtaille)
);

CREATE TABLE IF NOT EXISTS COLORIS (
  idcoloris DECIMAL(3,0),
  nomcoloris VARCHAR(30),
  PRIMARY KEY (idcoloris)
);

CREATE TABLE IF NOT EXISTS SNEAKER (
  idsneaker DECIMAL(8,0),
  nomsneaker VARCHAR(100),
  prix DECIMAL(4,0),
  idmarque DECIMAL(3,0) NOT NULL,
  idsexe DECIMAL(2,0) NOT NULL,
  idcoloris DECIMAL(3,0) NOT NULL,
  PRIMARY KEY (idsneaker),
  FOREIGN KEY (idmarque) REFERENCES MARQUE (idmarque),
  FOREIGN KEY (idsexe) REFERENCES SEXE (idsexe),
  FOREIGN KEY (idcoloris) REFERENCES COLORIS (idcoloris)
);

CREATE TABLE IF NOT EXISTS DESCRIPTION (
  iddescription DECIMAL(8,0),
  idsneaker DECIMAL(8,0),
  descriptionsneaker TEXT,
  PRIMARY KEY (iddescription,idsneaker),
  FOREIGN KEY (idsneaker) REFERENCES SNEAKER (idsneaker)
);

CREATE TABLE IF NOT EXISTS STOCK (
  idtaille DECIMAL(3,0),
  idsneaker DECIMAL(8,0),
  quantite DECIMAL(3,0),
  PRIMARY KEY (idtaille,idsneaker),
  FOREIGN KEY (idtaille) REFERENCES TAILLE (idtaille),
  FOREIGN KEY (idsneaker) REFERENCES SNEAKER (idsneaker)
);

CREATE TABLE IF NOT EXISTS PHOTO (
  idph DECIMAL(6,0),
  imgph MEDIUMBLOB,
  idsneaker DECIMAL(8,0),
  PRIMARY KEY (idph),
  FOREIGN KEY (idsneaker) REFERENCES SNEAKER (idsneaker)
);

CREATE TABLE IF NOT EXISTS FAVORI (
  idut DECIMAL(6,0),
  idsneaker DECIMAL(8,0),
  PRIMARY KEY (idut,idsneaker),
  FOREIGN KEY (idsneaker) REFERENCES SNEAKER (idsneaker),
  FOREIGN KEY (idut) REFERENCES UTILISATEUR (idut)
);

CREATE TABLE IF NOT EXISTS ACHAT (
  idut DECIMAL(6,0),
  idtaille DECIMAL(3,0),
  idsneaker DECIMAL(8,0),
  quantiteachat DECIMAL(2,0),
  PRIMARY KEY (idut,idtaille,idsneaker),
  FOREIGN KEY (idtaille) REFERENCES TAILLE (idtaille),
  FOREIGN KEY (idsneaker) REFERENCES SNEAKER (idsneaker),
  FOREIGN KEY (idut) REFERENCES UTILISATEUR (idut)
);

-- revoir table FAVORI + table ACHAT

-- revoir systeme de stock suivant les tailles et la paire