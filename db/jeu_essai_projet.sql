-- les roles
insert into ROLE(idRole,nomRole) values
	(1,'Administrateur'),
	(2,'Utilisateur');

-- les utilisateurs
insert into UTILISATEUR(idut,pseudout,emailut,mdput,activeut,idrole,photo) values
	(1,'adm1','adm1@orange.fr','adm1','O',1,null),
	(2,'adm2','adm2@orange.fr','adm2','O',1,null),
	(3,'adm3','adm3@univ-orleans.fr','adm3','O',1,null),
	(4,'foloife','foloife@free.fr','foloife','N',2,null),
	(5,'noa','noa@gmail.com','noa','O',2,null),
	(6,'quentin','quentin@gmail.com','quentin','O',2,null),
	(7,'korentin','korentin@gmail.com','korentin','O',2,null),
	(8,'lucas','lucas@gmail.com','lucas','O',2,null),
	(9,'hugo','hugo@gmail.com','hugo','O',2,null);

-- les marques
insert into MARQUE(idmarque, nommarque) values
	(1,'Nike'),
	(2,'Jordan'),
	(3,'Adidas'),
	(4,'New balance'),
    (5,'Converse'),
    (6, 'Autres');

-- les sexes
insert into SEXE(idsexe, nomsexe) values
	(1,'Hommes'),
	(2,'Femmes');

-- les tailles
insert into TAILLE(idtaille, taille) values
	(1,35),
	(2,36),
    (3,37),
    (4,38),
    (5,39),
    (6,40),
    (7,41),
    (8,42),
    (9,43),
    (10,44),
    (11,45),
    (12,46);

-- les coloris
insert into COLORIS(idcoloris, nomcoloris) values
	(1,"Blanc"),
	(2,"Noir"),
    (3,"Rouge"),
    (4,"Jaune"),
    (5,"Bleu"),
    (6,"Vert"),
    (7,"Orange"),
    (8,"Violet"),
    (9,"Rose"),
    (10,"Marron"),
    (11,"Gris"),
    (12,"Beige");

-- les sneakers
insert into SNEAKER(idsneaker,nomsneaker,prix,idmarque,idsexe,idcoloris) values
	(1,"Air Jordan 4 Seafoam", 210, 2, 1, 1),
    (2,"1906R Protection Pack Beige", 180, 4, 1, 12),
    (3,"Nike Air More Uptempo Valentine’s Day", 180, 1, 2, 10),
    (4,"Jordan 2 Lucky Green", 190, 2, 1, 1),
    (5,"Forum Buckle Low Bad Bunny Blue Tint", 160, 3, 1, 5),
    (6,"Nike Dunk Low Next Nature White Mint", 160, 1, 2, 6),
    (7,"Air Jordan 4 Retro Thunder", 215, 2, 1, 2),
    (8,"Nike Dunk Low Active Fuchsia", 165, 1, 2, 9),
    (9,"New Balance 550 White Nightwatch Green", 195, 4, 1, 1),
    (10,"Adidas Yeezy Slide Onyx", 120, 3, 1, 2),
    (11,"Converse Stüssy Chuck 70 Fossil", 195, 5, 2, 9),
    (12,"Air Jordan 1 Low SE Reverse Ice Blue", 165, 2, 1, 1),
    (13,"Air Jordan 4 Canyon Purple", 240, 2, 1, 8),
	(14,"2002R Protection Pack Rain Cloud", 280, 4, 1, 11),
    (15,"Off-White x Futura x Nike Dunk Low UNC", 2800, 1, 1, 5),
    (16,"Big Red Boots", 250, 6, 2, 3);

-- les descriptions des sneakers
insert into DESCRIPTION(iddescription,idsneaker,descriptionsneaker) values
    (1,1,"Jordan Brand va réjouir les fans de la Jordan 4 en début d'année 2023 avec la sortie d'un nouveau coloris épuré."),
    (2,1,"La Air Jordan 4 Seafoam affiche une empeigne en cuir blanc accompagnée de détails noirs comme les rabats et le heeltab. La cage en TPU est bien présente sur le panneau latéral dans une teinte blanche. Quelques touches de vert clair sont parsemées sur la midsole, les oeillets en TPU, les brandings et la doublure."),
    (3,1,"Légère, cette nouvelle itération de la AJ4 mise sur la douceur pour se marier à toutes vos tenues."),
    (1,2,"New Balance fait son retour avec une nouvelle collection de son Protection Pack à l'aspect déconstruit, mais cette fois-ci sur une nouvelle silhouette, la 1906R."),
    (2,2,"La New Balance 1906R Protection Pack Beige affiche une base en mesh beige accompagnée d'empiècements en suède et hairy suède ton-sur-ton dans un style déstructuré. Un logo N-Lock est apposé sur le panneau latéral tandis qu'une semelle noire ABZORB et N-Ergy, qui n'est pas sans rappeler celle de la 2002R, assure un confort optimal."),
    (1,3,"Cette année pour la Saint-Valentin, Nike voit les choses en grand avec une nouvelle version glamour de l'iconique hightop !");

-- les stocks disponibles par taille
insert into STOCK(idtaille,idsneaker,quantite) values
    (7,1,3),
    (8,1,2),
    (9,1,8),
    (10,1,1),
    (7,2,2); 

-- les photos des sneakers
insert into PHOTO(idph,imgph,idsneaker) values
    (1,null,1),
    (2,null,2),
    (3,null,3),
    (4,null,4),
    (5,null,5),
    (6,null,6),
    (7,null,7),
    (8,null,8),
    (9,null,9),
    (10,null,10),
    (11,null,11),
    (12,null,12),
    (13,null,13),
    (14,null,14),
    (15,null,1),
    (16,null,1),
    (17,null,2);

-- les sneakers en favoris
insert into FAVORI(idut,idsneaker) values
    (1,1),
    (1,2),
    (1,3),
    (1,4),
    (1,5),
    (1,6),
    (1,7),
    (1,8),
    (1,9),
    (1,10),
    (1,11),
    (1,12),
    (1,13),
    (2,1);