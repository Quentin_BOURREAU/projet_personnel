CREATE TABLE IF NOT EXISTS CATEGORIE (
idcat decimal(3,0),
nomcat varchar(30),
PRIMARY KEY (idcat)
);

CREATE TABLE IF NOT EXISTS STATUT (
idst char,
nomst varchar(30),
PRIMARY KEY (idst)
);

CREATE TABLE IF NOT EXISTS ROLE (
idrole decimal(2,0),
nomrole varchar(30),
PRIMARY KEY (idrole)
);

CREATE TABLE IF NOT EXISTS UTILISATEUR (
idut decimal(6,0),
pseudout varchar(20) unique,
emailut varchar(100),
mdput varchar(100),
biographie text,
photo mediumblob,
activeut char,
idrole decimal(2,0),
PRIMARY KEY (idut),
FOREIGN KEY (idrole) REFERENCES ROLE (idrole)
);


CREATE TABLE IF NOT EXISTS OBJET (
idob decimal(6,0),
nomob varchar(30),
descriptionob text,
idut decimal(6,0),
idcat decimal(3,0),
PRIMARY KEY (idob),
FOREIGN KEY (idut) REFERENCES UTILISATEUR (idut),
FOREIGN KEY (idcat) REFERENCES CATEGORIE (idcat)
);

CREATE TABLE IF NOT EXISTS PHOTO (
idph decimal(6,0),
titreph varchar(50),
imgph mediumblob,
idob decimal(6,0),
PRIMARY KEY (idph),
FOREIGN KEY (idob) REFERENCES OBJET (idob)
);


CREATE TABLE IF NOT EXISTS VENTE (
idve decimal(8,0),
prixbase decimal(8,2),
prixmin decimal(8,2),
debutve datetime,
finve datetime,
idob decimal(6,0),
idst char,
PRIMARY KEY (idve),
FOREIGN KEY (idob) REFERENCES OBJET (idob),
FOREIGN KEY (idst) REFERENCES STATUT (idst)
);

CREATE TABLE IF NOT EXISTS ENCHERIR (
idut decimal(6,0),
idve decimal(8,0),
dateheure datetime,
montant decimal(8,2),
PRIMARY KEY (idut, idve, dateheure),
FOREIGN KEY (idve) REFERENCES VENTE (idve),
FOREIGN KEY (idut) REFERENCES UTILISATEUR (idut)
);

CREATE TABLE IF NOT EXISTS FAVORIS (
idut decimal(6,0),
idve decimal(8,0),
PRIMARY KEY (idut,idve),
FOREIGN KEY (idve) REFERENCES VENTE (idve),
FOREIGN KEY (idut) REFERENCES UTILISATEUR (idut)
);

CREATE TABLE IF NOT EXISTS NOTE (
idnote decimal(8,0),
idut decimal(6,0),
nbetoiles decimal(1,0),
PRIMARY KEY (idnote),
FOREIGN KEY (idut) REFERENCES UTILISATEUR (idut)
);












