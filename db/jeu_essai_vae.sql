-- les roles
insert into ROLE(idRole,nomRole) values
	(1,'Administrateur'),
	(2,'Utilisateur');

-- les catégories
insert into CATEGORIE(idCat,nomCat) values
	(1,'Meubles'),
	(2,'Jouets'),
	(3,'Art'),
	(4,'High-tech'),
    (5,'Culture');


-- les statuts
insert into STATUT(idSt,nomSt) values
	(1,'A venir'),
	(2,'En cours'),
	(3,'A valider'),
	(4,'Validée'),
	(5,'Non conclue');
	
-- les utilisateurs
insert into UTILISATEUR(idUt,pseudoUt,emailUT,mdpUt,activeUt,idRole,biographie,photo) values
	(1,'adm1','adm1@orange.fr','adm1','O',1,"Admin de test 1","null"),
	(2,'adm2','adm2@orange.fr','adm2','O',1,"Admin de test 2","null"),
	(3,'adm3','adm3@univ-orleans.fr','adm3','O',1,"Admin de test 3","null"),
	(4,'foloife','foloife@free.fr','foloife','O',2,"Utilisateur de test","null"),
	(1002,'noah','noah@gmail.com','noah','O',2,"Noah Jacquet","null"),
	(1003,'quentin','quentin@gmail.com','quentin','O',2,"Quentin Bourreau","null"),
	(1004,'korentin','korentin@gmail.com','korentin','O',2,"Korentin Georget","korentin"),
	(1005,'lucas','lucas@gmail.com','lucas','O',2,"Lucas Devers-Doré","null"),
	(1006,'hugo','hugo@gmail.com','hugo','O',2,"Hugo Sainson","null");

insert into OBJET(idOb,nomOb,descriptionOb,idCat,idUt) values
	(510,'Scie comme neuve','Scie comme neuveLorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus.',4,1002),
	(511,'Chemise jamais servie','Chemise jamais servieLorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus.',1,1003),
	(512,'Pantalon jaune','Pantalon jauneLorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus.',1,1004),
	(513,'Chapeau rouge','Chapeau rougeLorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus.',1,1005),
	(514,'Chemise rouge','Chemise rougeLorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus.',1,1006),
	(515,'Moto bleu','Moto bleu Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus.',4,1006);

insert into VENTE(idVe,prixBase,prixMin,debutVe,finVe,idSt,idOb) values
	(510,73,200,STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),DATE_ADD(STR_TO_DATE('1/2/2025:10:00:00','%d/%m/%Y:%h:%i:%s'), INTERVAL 6 DAY),2,510),
	(511,505,690,STR_TO_DATE('7/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),DATE_ADD(STR_TO_DATE('7/2/2025:10:00:00','%d/%m/%Y:%h:%i:%s'), INTERVAL 4 DAY),2,511),
	(512,202,388,STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),DATE_ADD(STR_TO_DATE('14/2/2025:10:00:00','%d/%m/%Y:%h:%i:%s'), INTERVAL 3 DAY),2,512),
	(513,2,3,STR_TO_DATE('21/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),DATE_ADD(STR_TO_DATE('21/2/2025:10:00:00','%d/%m/%Y:%h:%i:%s'), INTERVAL 5 DAY),2,513),
	(514,503,942,STR_TO_DATE('28/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),DATE_ADD(STR_TO_DATE('28/2/2025:10:00:00','%d/%m/%Y:%h:%i:%s'), INTERVAL 7 DAY),2,514),
	(515,10,20,STR_TO_DATE('23/6/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),STR_TO_DATE('23/6/2023:10:33:00','%d/%m/%Y:%h:%i:%s'),2,515);

insert into PHOTO(idph,titreph,imgph,idob) values
	(1,'Pantalon Jaune',"pantalonJaune",512);

insert into FAVORIS(idut,idve) values
	(1,510),
	(1,511),
	(1,512),
	(1,514),
	(2,510),
	(2,511),
	(2,514),
	(2,513),
	(3,510),
	(3,511);


insert into NOTE(idnote,idut,nbetoiles) values
	(1,1004,3),
	(2,1004,5),
	(3,1004,3),
	(4,1004,2);

insert into ENCHERIR(idUT,idVe,dateheure,montant) values
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 292 minute),127),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 379 minute),184),
	(1002,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 845 minute),197),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1018 minute),200),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1462 minute),271),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1523 minute),280),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1900 minute),298),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2002 minute),317),
	(1002,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2302 minute),356),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2497 minute),422),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2869 minute),446),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 3354 minute),481),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 3684 minute),505),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 4039 minute),533),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 4351 minute),572),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 4394 minute),586),
	(3,510,date_add(STR_TO_DATE('1/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 4667 minute),588),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 76 minute),340),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 146 minute),397),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 159 minute),449),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 171 minute),583),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 253 minute),776),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 402 minute),967),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 574 minute),1131),
	(1002,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 619 minute),1141),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 643 minute),1330),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 823 minute),1517),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 862 minute),1573),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 877 minute),1720),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 951 minute),1885),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1060 minute),2047),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1220 minute),2059),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1370 minute),2218),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1444 minute),2296),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1447 minute),2458),
	(3,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1572 minute),2582),
	(1002,512,date_add(STR_TO_DATE('14/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1590 minute),2758),
	(3,513,date_add(STR_TO_DATE('21/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 887 minute),3),
	(3,513,date_add(STR_TO_DATE('21/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1275 minute),4),
	(3,513,date_add(STR_TO_DATE('21/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1466 minute),6),
	(3,513,date_add(STR_TO_DATE('21/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2186 minute),8),
	(3,513,date_add(STR_TO_DATE('21/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2979 minute),10),
	(3,513,date_add(STR_TO_DATE('21/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 3214 minute),11),
	(3,513,date_add(STR_TO_DATE('21/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 3551 minute),13),
	(1002,514,date_add(STR_TO_DATE('28/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 3777 minute),873),
	(3,514,date_add(STR_TO_DATE('28/2/2023:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 5391 minute),1031);
