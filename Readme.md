# Projet personnel

Ce projet consiste à développer une application de ventes de sneakers (chaussures) en ligne comme Wethenew.

## Setup 🖥️

Clonez premièrement le dépot avec la commande `git clone https://gitlab.com/Quentin_BOURREAU/projet_personnel`
Si l'emplacement de la librairie Javafx a été modifiée sur votre ordinateur, changez ceci dans la commande `javac` et `java` dans le fichier de lancement.

### Mac 🍎 / Linux 🐧

Lancez le fichier launch.sh présent dans la racine du dépot git. `./projet_personnel/lancement.sh`

## Outils utililisées 📚
### Java FX ☕

J'utilise la librairie javafx pour avoir une interface graphique sur l'application développée.

### Base de donnée 💾

J'ai utilisé une base de données locale.
